#!/usr/bin/env sh
if [ ! -L "$0" ]; then
  electron_dir="$(dirname "$0")"
else
  if command -v readlink >/dev/null; then
    electron_dir="$(dirname "$(readlink -f "$0")")"
  else
    electron_dir="/opt/{{PRODUCT_NAME}}"
  fi
fi
electron_path="${electron_dir}/{{REAL_BINARY_NAME}}"
if [ -n "${WAYLAND_DISPLAY+1}" ]; then
  exec "${electron_path}" --enable-features=WaylandWindowDecorations,WebRTCPipeWireCapturer --ozone-platform=wayland "$@"
else
  exec "${electron_path}" --ozone-platform=x11 "$@"
fi
