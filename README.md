- [Sophie](#sophie)
  - [Development](#development)
  - [License](#license)

# Sophie

_Messaging app built from the ground up with security in mind_

This is currently a work in progress, you should probably check back later.

## Development

The codebase is built using [nodejs](https://nodejs.org/) `>=16.13.x`, and uses [yarn](https://yarnpkg.com/) (version `3.1.1`) to manage its dependencies.
The project structure is based on [Vite Electron Builder Boilerplate](https://github.com/cawa-93/vite-electron-builder) but it was changed to incorporate [react](https://reactjs.org/) and [mobx-state-tree](https://github.com/mobxjs/mobx-state-tree).

After installing `node`, you can install `yarn` with

```sh
npm i -g yarn
```

To start working, install all dependencies with

```sh
yarn install --immutable
```

If TypeScript complains about missing type definitions, run

```sh
yarn types
```

To start a development instance of Sophie, which will reload on source changes, run

```sh
yarn watch
```

To run all the tests, run

```sh
yarn test
```

or to automatically rerun tests for changed source files, run

```sh
yarn watch:test
```

To build the application in release mode, run

```sh
yarn compile
```

To typecheck the project, run

```sh
yarn typecheck
```

To run the linter, run

```sh
yarn lint
```

## License

> Copyright (C) 2021-2022 Kristóf Marussy &lt;kristof@marussy.com&gt;<br>
> Copyright (C) 2022 Vijay A &lt;vraravam@users.noreply.github.com&gt;
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, version 3.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
> GNU Affero General Public License for more details.
> You should have received a copy of the GNU Affero General Public License
> along with this program. If not, see &lt;[https://www.gnu.org/licenses/](https://www.gnu.org/licenses/)&gt;.
