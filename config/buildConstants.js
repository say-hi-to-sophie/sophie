import { readFileSync } from 'node:fs';
import path from 'node:path';

import fileUrlToDirname from './fileUrlToDirname.js';

const thisDir = fileUrlToDirname(import.meta.url);

// We import this from a vite config, where top-level await is not available (es2021),
// so we have to use the synchronous filesystem API.
const electronVendorsJson = readFileSync(
  path.join(thisDir, '../.electron-vendors.cache.json'),
  'utf8',
);

/** @type {{ chrome: number; node: number; }} */
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
const { chrome: chromeVersion, node: nodeVersion } =
  JSON.parse(electronVendorsJson);

/** @type {string} */
export const banner = `/*!
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 * Copyright (C)  2022 Vijay A <vraravam@users.noreply.github.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
`;

/** @type {string} */
export const chrome = `chrome${chromeVersion}`;

/** @type {string} */
export const node = `node${nodeVersion}`;
