/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

const path = require('node:path');

const { flipFuses } = require('@electron/fuses');

/**
 * Hardens the shipped electron binary by burning some electron fuses.
 *
 *
 * @param {import('electron-builder').AfterPackContext} context The `electron-builder` context.
 * @param {import('@electron/fuses').FuseConfig<boolean>} config The fuses to burn.
 * @return {Promise<void>} The promise to flip the fuses.
 * @see https://github.com/electron/fuses
 */
module.exports = async function burnFuses(context, config) {
  /** @type {string} */
  const ext =
    {
      darwin: '.app',
      win32: '.exe',
    }[context.electronPlatformName] || '';
  const electronBinaryPath = path.join(
    context.appOutDir,
    `${context.packager.appInfo.productFilename}${ext}`,
  );
  /** @type {import('@electron/fuses').FuseConfig<boolean>} */
  return flipFuses(electronBinaryPath, config);
};
