/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { readFile } from 'node:fs/promises';

/** @type {string} */
const PLUGIN_NAMESPACE = 'sophie-src';

/** @type {RegExp} */
// eslint-disable-next-line security/detect-non-literal-regexp -- Argument is const.
const FILTER_REGEXP = new RegExp(`^${PLUGIN_NAMESPACE}:(.+)$`);

/**
 * An esbuild plugin that import the source of referenced file or package as a string.
 *
 * Prefix the imported file name with `sophie-src:` to import its source instead.
 *
 * @type { import('esbuild').Plugin }
 */
const srcPlugin = {
  name: 'sophie-inline-service-inject-plugin',
  setup(build) {
    build.onResolve(
      {
        filter: FILTER_REGEXP,
      },
      async ({ path: pathToResolve, importer, resolveDir }) => {
        const match = FILTER_REGEXP.exec(pathToResolve);
        if (match === null) {
          return {
            path: '',
            namespace: PLUGIN_NAMESPACE,
            errors: [
              {
                text: `Could not resolve ${pathToResolve}`,
              },
            ],
          };
        }
        const { path, errors, warnings } = await build.resolve(match[1], {
          importer,
          resolveDir,
          kind: 'import-statement',
        });
        return {
          path,
          namespace: PLUGIN_NAMESPACE,
          errors,
          warnings,
        };
      },
    );
    build.onLoad(
      {
        filter: /./,
        namespace: PLUGIN_NAMESPACE,
      },
      async ({ path }) => {
        const src = await readFile(path, 'utf8');
        return {
          contents: `export default ${JSON.stringify(src)};`,
        };
      },
    );
  },
};

export default srcPlugin;
