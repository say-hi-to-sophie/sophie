const { readFileSync } = require('node:fs');
const path = require('node:path');

const { transformSync } = require('esbuild');

const electronVendorsJson = readFileSync(
  path.join(__dirname, '../.electron-vendors.cache.json'),
  'utf8',
);

/** @type {{ node: number; }} */
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
const { node: nodeVersion } = JSON.parse(electronVendorsJson);

/**
 * @param {string} source
 * @param {string} filePath
 * @return {import('@jest/transform').TransformedSource}
 */
function process(source, filePath) {
  /** @type {import('esbuild').Loader} */
  let loader;
  if (filePath.endsWith('tsx')) {
    loader = 'tsx';
  } else if (filePath.endsWith('ts')) {
    loader = 'ts';
  } else {
    loader = 'js';
  }
  const { code, map } = transformSync(source, {
    loader,
    sourcefile: filePath,
    format: 'cjs',
    target: `node${nodeVersion}`,
    sourcemap: true,
    define: {
      __DEV__: JSON.stringify(false), // For mobx
      'process.env.NODE_ENV': 'test',
      'process.env.MODE': 'test',
      'import.meta.env': JSON.stringify({
        DEV: false,
        MODE: 'test',
        PROD: true,
      }),
    },
  });
  return {
    code,
    map,
  };
}

/** @type {import('@jest/transform').Transformer<void>} */
module.exports = {
  canInstrument: false,
  process,
};
