const { app } = require('electron');

module.exports = () => app.whenReady();
