const path = require('node:path');

/** @type {import('@jest/types').Config.InitialOptions} */
module.exports = {
  transform: {
    '\\.[jt]sx?$': path.join(__dirname, 'jestEsbuildCjsTransformer.cjs'),
  },
  testRegex: '(\\.|/)integ\\.test\\.ts$',
  transformIgnorePatterns: ['/node_modules/(?!(chalk|lodash-es)/)'],
  moduleNameMapper: {
    '^@sophie/(.+)$': path.join(__dirname, '../packages/$1/src/index.ts'),
    '^(\\.{1,2}/.*)\\.jsx?$': '$1',
    // Workaround for jest to recognize the vendored dependencies of chalk.
    '^#ansi-styles$': path.join(
      __dirname,
      '../node_modules/chalk/source/vendor/ansi-styles/index.js',
    ),
    '^#supports-color$': path.join(
      __dirname,
      '../node_modules/chalk/source/vendor/supports-color/index.js',
    ),
  },
  resetMocks: true,
  restoreMocks: true,
  testEnvironment: 'node',
  testPathIgnorePatterns: ['/dist/', '/node_modules/'],
};
