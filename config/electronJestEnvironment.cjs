const electron = require('electron');
const { TestEnvironment } = require('jest-environment-node');

module.exports = class ElectronEnvironment extends TestEnvironment {
  /** @override */
  async setup() {
    await super.setup();
    this.global.ELECTRON_MODULE = electron;
  }
};
