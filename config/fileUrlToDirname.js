import path from 'node:path';
import { fileURLToPath } from 'node:url';

/**
 * @param {string} url
 * @returns {string}
 */
export default function fileUrlToDirname(url) {
  return path.dirname(fileURLToPath(url));
}
