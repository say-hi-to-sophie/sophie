import path from 'node:path';

import fileUrlToDirname from './fileUrlToDirname.js';

const thisDir = fileUrlToDirname(import.meta.url);

/** @type {import('@jest/types').Config.InitialOptions} */
export default {
  transform: {
    '\\.tsx?$': path.join(thisDir, 'jestEsbuildTransformer.js'),
  },
  testRegex: '((?<!integ)\\.|/)test\\.tsx?$',
  extensionsToTreatAsEsm: ['.ts', '.tsx'],
  moduleNameMapper: {
    '^@sophie/(.+)$': path.join(thisDir, '../packages/$1/src/index.ts'),
    '^(\\.{1,2}/.*)\\.jsx?$': '$1',
    // Workaround for jest to recognize the vendored dependencies of chalk.
    '^#ansi-styles$': path.join(
      thisDir,
      '../node_modules/chalk/source/vendor/ansi-styles/index.js',
    ),
    '^#supports-color$': path.join(
      thisDir,
      '../node_modules/chalk/source/vendor/supports-color/index.js',
    ),
  },
  resetMocks: true,
  restoreMocks: true,
  testEnvironment: 'node',
  testPathIgnorePatterns: ['/dist/', '/node_modules/'],
};
