import { transform } from 'esbuild';

import { node } from './buildConstants.js';

/**
 * @param {string} source
 * @param {string} filePath
 * @return {Promise<import('@jest/transform').TransformedSource>}
 */
async function processAsync(source, filePath) {
  const { code, map } = await transform(source, {
    loader: filePath.endsWith('tsx') ? 'tsx' : 'ts',
    sourcefile: filePath,
    format: 'esm',
    target: node,
    sourcemap: true,
    define: {
      __DEV__: JSON.stringify(false), // For mobx
      'process.env.NODE_ENV': 'test',
      'process.env.MODE': 'test',
      'import.meta.env': JSON.stringify({
        DEV: false,
        MODE: 'test',
        PROD: true,
      }),
    },
  });
  return {
    code,
    map,
  };
}

/** @type {import('@jest/transform').AsyncTransformer<void>} */
export default {
  canInstrument: false,
  processAsync,
};
