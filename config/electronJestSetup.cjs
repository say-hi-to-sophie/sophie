const electron = /** @type {{ ELECTRON_MODULE: import('electron')}} */ (
  /** @type {unknown} */ (globalThis)
).ELECTRON_MODULE;

const { BrowserWindow } = electron;

jest.mock('electron', () => electron);

afterEach(() => {
  BrowserWindow.getAllWindows().forEach((window) => {
    window.destroy();
  });
});
