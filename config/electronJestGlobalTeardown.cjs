const { app } = require('electron');

/** @type {(globalConfig: import('@jest/types').Config.InitialOptions) => void} */
module.exports = (globalConfig) => {
  if (!globalConfig.watch && !globalConfig.watchAll) {
    app.quit();
  }
};
