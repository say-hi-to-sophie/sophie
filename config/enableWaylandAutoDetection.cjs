/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

const { readFile, rename, writeFile } = require('node:fs/promises');
const path = require('node:path');

const templatePath = path.join(__dirname, '../build-helpers/detect_wayland.sh');

/**
 * Replaces the `{{handlebars}}` in the template with the replacements.
 *
 * Each `{{VARIABLE}}` will be repalced by `value` in the replacement `['VARIABLE', value]`.
 *
 * @param {string} template The template.
 * @param {[string, string][]} replacements The replacements to apply.
 * @returns
 */
function executeTemplate(template, ...replacements) {
  const replacementsMap = new Map(replacements);
  return template.replaceAll(
    /{{([^}]+)}}/g,
    (_match, /** @type {string} */ variable) => {
      const replacement = replacementsMap.get(variable);
      if (replacement === undefined) {
        throw new Error(`Unknown variable: ${variable}`);
      }
      return replacement;
    },
  );
}

/**
 * Adds a wrapper scripts that detects in wayland is in use and enabled it in chromium.
 *
 * The script in `build-heleprs/detect_wayland.sh` uses the `WAYLAND_DISPLAY` environmental
 * variable to detect whether wayland is in use.
 *
 * If wayland is in use, the script enables the wayland ozone backed for chromium
 * and pipewire screen sharing. Otherwise, the x11 ozone backend will be used.
 *
 * @param {import('electron-builder').AfterPackContext} context The `electron-builder` context.
 * @return {Promise<void>} The promise to add the wrapper script.
 * @see https://stackoverflow.com/a/45537237
 */
module.exports = async function enableWaylandAutoDetection(context) {
  const {
    appOutDir,
    packager: {
      appInfo: { productName, productFilename },
    },
  } = context;

  const electronBinaryPath = path.join(appOutDir, productFilename);
  const newFilename = `${productFilename}-bin`;
  const newElectronBinaryPath = path.join(appOutDir, newFilename);

  await rename(electronBinaryPath, newElectronBinaryPath);

  const wrapperScriptTempate = await readFile(templatePath, 'utf8');
  const wrapperScript = executeTemplate(
    wrapperScriptTempate,
    ['PRODUCT_NAME', productName],
    ['REAL_BINARY_NAME', newFilename],
  );
  await writeFile(electronBinaryPath, wrapperScript, {
    encoding: 'utf8',
    mode: 0o755,
  });
};
