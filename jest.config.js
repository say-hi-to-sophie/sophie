/** @type {import('@jest/types').Config.InitialOptions} */
export default {
  projects: ['<rootDir>/packages/*/jest.config.js'],
  collectCoverageFrom: [
    '**/*.{ts,tsx}',
    '**/*.{test,spec}.{ts,tsx}',
    '!**/__fixtures__/**/*',
  ],
  /** @type {'v8'} */
  coverageProvider: 'v8',
  /** @type {['cobertura', 'text']} */
  coverageReporters: ['cobertura', 'text'],
  coverageDirectory: 'coverage/test',
};
