/** @type {import('@jest/types').Config.InitialOptions} */
module.exports = {
  projects: ['<rootDir>/packages/*/jest.integ.config.cjs'],
  collectCoverageFrom: [
    '**/*.{ts,tsx}',
    '!**/*.{test,spec}.{ts,tsx}',
    '!**/__fixtures__/**/*',
  ],
  /** @type {'v8'} */
  coverageProvider: 'v8',
  /** @type {['cobertura', 'text']} */
  coverageReporters: ['cobertura', 'text'],
  coverageDirectory: 'coverage/integ',
};
