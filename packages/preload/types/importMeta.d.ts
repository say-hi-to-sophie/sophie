interface ImportMeta {
  env: {
    DEV: boolean;
    MODE: string;
    PROD: boolean;
  };
}
