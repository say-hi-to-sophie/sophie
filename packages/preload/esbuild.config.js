import { chrome } from '../../config/buildConstants.js';
import fileUrlToDirname from '../../config/fileUrlToDirname.js';
import getEsbuildConfig from '../../config/getEsbuildConfig.js';

export default getEsbuildConfig({
  absWorkingDir: fileUrlToDirname(import.meta.url),
  entryPoints: ['src/index.ts'],
  outfile: 'dist/index.cjs',
  format: 'cjs',
  platform: 'node',
  target: chrome,
  sourcemap: 'inline',
  external: ['electron'],
});
