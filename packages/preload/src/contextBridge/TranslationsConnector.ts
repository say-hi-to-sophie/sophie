/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import {
  MainToRendererIpcMessage,
  RendererToMainIpcMessage,
  Translation,
} from '@sophie/shared';
import { ipcRenderer } from 'electron';
import type { ResourceKey } from 'i18next';
import log from 'loglevel';

export default class TranslationsConnector {
  private listener: (() => void) | undefined;

  constructor(private readonly devMode: boolean) {
    ipcRenderer.on(MainToRendererIpcMessage.ReloadTranslations, () => {
      try {
        this.listener?.();
      } catch (error) {
        log.error('Translations listener onReloadTranslations failed', error);
        this.listener = undefined;
      }
    });
  }

  onReloadTranslations(listener: () => void): void {
    if (!this.devMode) {
      throw new Error(
        'Translation reloading is only supported in development mode',
      );
    }
    this.listener = listener;
  }
}

export async function getTranslation(
  translation: Translation,
): Promise<ResourceKey> {
  const parsedTranslation = Translation.parse(translation);
  try {
    // We don't have any way to validate translations,
    // but they are coming from a trusted source and will be validated
    // in the renderer anyways, so we should be fine.
    return (await ipcRenderer.invoke(
      RendererToMainIpcMessage.GetTranslation,
      parsedTranslation,
    )) as ResourceKey;
  } catch (error) {
    const message = 'Failed to get translation';
    log.error(message, translation, error);
    throw new Error(message);
  }
}
