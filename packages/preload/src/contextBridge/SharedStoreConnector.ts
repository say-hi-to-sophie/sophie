/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import {
  Action,
  MainToRendererIpcMessage,
  RendererToMainIpcMessage,
  SharedStoreListener,
  SharedStoreSnapshotIn,
} from '@sophie/shared';
import { ipcRenderer } from 'electron';
import log from 'loglevel';
import type { IJsonPatch } from 'mobx-state-tree';

export default class SharedStoreConnector {
  private onSharedStoreChangeCalled = false;

  private listener: SharedStoreListener | undefined;

  constructor(private readonly allowReplaceListener: boolean) {
    ipcRenderer.on(
      MainToRendererIpcMessage.SharedStorePatch,
      (_event, patch) => {
        try {
          // `mobx-state-tree` will validate the patch, so we can safely cast here.
          this.listener?.onPatch(patch as IJsonPatch[]);
        } catch (error) {
          log.error('Shared store listener onPatch failed', error);
          this.listener = undefined;
        }
      },
    );
  }

  async onSharedStoreChange(listener: SharedStoreListener): Promise<void> {
    if (this.onSharedStoreChangeCalled && !this.allowReplaceListener) {
      throw new Error('Shared store change listener was already set');
    }
    this.onSharedStoreChangeCalled = true;
    let success = false;
    let snapshot: unknown;
    try {
      snapshot = await ipcRenderer.invoke(
        RendererToMainIpcMessage.GetSharedStoreSnapshot,
      );
      success = true;
    } catch (error) {
      log.error('Failed to get initial shared store snapshot', error);
    }
    if (!success) {
      throw new Error('Failed to connect to shared store');
    }
    // `mobx-state-tree` will validate the snapshot, so we can safely cast here.
    listener.onSnapshot(snapshot as SharedStoreSnapshotIn);
    this.listener = listener;
  }
}

export function dispatchAction(actionToDispatch: Action): void {
  // Let the full zod parse error bubble up to the main world,
  // since all data it may contain was provided from the main world.
  const parsedAction = Action.parse(actionToDispatch);
  try {
    ipcRenderer.send(RendererToMainIpcMessage.DispatchAction, parsedAction);
  } catch (error) {
    // Do not leak IPC failure details into the main world.
    const message = 'Failed to dispatch action';
    log.error(message, actionToDispatch, error);
    throw new Error(message);
  }
}
