/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { jest } from '@jest/globals';
import {
  Action,
  MainToRendererIpcMessage,
  RendererToMainIpcMessage,
  SharedStoreSnapshotIn,
  SophieRenderer,
} from '@sophie/shared';
import { mocked } from 'jest-mock';
import log from 'loglevel';
import type { IJsonPatch } from 'mobx-state-tree';

jest.unstable_mockModule('electron', () => ({
  ipcRenderer: {
    invoke: jest.fn(),
    on: jest.fn(),
    send: jest.fn(),
  },
}));

const { ipcRenderer } = await import('electron');

const { default: createSophieRenderer } = await import(
  '../createSophieRenderer.js'
);

const event: Electron.IpcRendererEvent =
  undefined as unknown as Electron.IpcRendererEvent;

const snapshot: SharedStoreSnapshotIn = {
  shouldUseDarkColors: true,
};

const patch: IJsonPatch[] = [
  {
    op: 'replace',
    path: 'foo',
    value: 'bar',
  },
];

const action: Action = {
  action: 'set-theme-source',
  themeSource: 'dark',
};

const invalidAction = {
  action: 'not-a-valid-action',
} as unknown as Action;

beforeAll(() => {
  log.disableAll();
});

describe('createSophieRenderer', () => {
  test('registers a shared store patch listener', () => {
    createSophieRenderer(false);
    expect(ipcRenderer.on).toHaveBeenCalledWith(
      MainToRendererIpcMessage.SharedStorePatch,
      expect.anything(),
    );
  });
});

describe('SharedStoreConnector', () => {
  let sut: SophieRenderer;
  let onSharedStorePatch: (
    eventArg: Electron.IpcRendererEvent,
    patchArg: unknown,
  ) => void;
  const listener = {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars
    onSnapshot: jest.fn((_snapshot: SharedStoreSnapshotIn) => {}),
    // eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars
    onPatch: jest.fn((_patch: IJsonPatch[]) => {}),
  };

  beforeEach(() => {
    sut = createSophieRenderer(false);
    [, onSharedStorePatch] = mocked(ipcRenderer.on).mock.calls.find(
      ([channel]) => channel === MainToRendererIpcMessage.SharedStorePatch,
    )!;
  });

  describe('onSharedStoreChange', () => {
    test('requests a snapshot from the main process', async () => {
      mocked(ipcRenderer.invoke).mockResolvedValueOnce(snapshot);
      await sut.onSharedStoreChange(listener);
      expect(ipcRenderer.invoke).toHaveBeenCalledWith(
        RendererToMainIpcMessage.GetSharedStoreSnapshot,
      );
      expect(listener.onSnapshot).toHaveBeenCalledWith(snapshot);
    });

    test('catches IPC errors without exposing them', async () => {
      mocked(ipcRenderer.invoke).mockRejectedValue(new Error('s3cr3t'));
      await expect(
        sut.onSharedStoreChange(listener),
      ).rejects.not.toHaveProperty('message', expect.stringMatching(/s3cr3t/));
      expect(listener.onSnapshot).not.toHaveBeenCalled();
    });
  });

  describe('dispatchAction', () => {
    test('dispatches valid actions', () => {
      sut.dispatchAction(action);
      expect(ipcRenderer.send).toHaveBeenCalledWith(
        RendererToMainIpcMessage.DispatchAction,
        action,
      );
    });

    test('does not dispatch invalid actions', () => {
      expect(() => sut.dispatchAction(invalidAction)).toThrow();
      expect(ipcRenderer.send).not.toHaveBeenCalled();
    });
  });

  describe('when no listener is registered', () => {
    test('discards the received patch without any error', () => {
      expect(() => onSharedStorePatch(event, patch)).not.toThrow();
    });
  });

  function testRefusesToRegisterAnotherListener(): void {
    test('refuses to register another listener', async () => {
      await expect(sut.onSharedStoreChange(listener)).rejects.toBeInstanceOf(
        Error,
      );
    });
  }

  function testDoesNotPassPatchesToTheListener(
    name = 'does not pass patches to the listener',
  ): void {
    // eslint-disable-next-line jest/valid-title -- Title is a string parameter.
    test(name, () => {
      onSharedStorePatch(event, patch);
      expect(listener.onPatch).not.toHaveBeenCalled();
    });
  }

  describe('when a listener registered successfully', () => {
    beforeEach(async () => {
      mocked(ipcRenderer.invoke).mockResolvedValueOnce(snapshot);
      await sut.onSharedStoreChange(listener);
    });

    test('passes patches to the listener', () => {
      onSharedStorePatch(event, patch);
      expect(listener.onPatch).toHaveBeenCalledWith(patch);
    });

    test('catches listener errors', () => {
      mocked(listener.onPatch).mockImplementation(() => {
        throw new Error('listener error');
      });
      expect(() => onSharedStorePatch(event, patch)).not.toThrow();
    });

    testRefusesToRegisterAnotherListener();

    describe('after the listener threw in onPatch', () => {
      beforeEach(() => {
        mocked(listener.onPatch).mockImplementation(() => {
          throw new Error('listener error');
        });
        onSharedStorePatch(event, patch);
        listener.onPatch.mockRestore();
      });

      testDoesNotPassPatchesToTheListener('does not pass on patches any more');
    });
  });

  describe('when a listener failed to register due to IPC error', () => {
    beforeEach(async () => {
      mocked(ipcRenderer.invoke).mockRejectedValue(
        new Error('ipcRenderer error'),
      );
      try {
        await sut.onSharedStoreChange(listener);
      } catch {
        // Ignore error.
      }
    });

    testRefusesToRegisterAnotherListener();

    testDoesNotPassPatchesToTheListener();
  });

  describe('when a listener failed to register due to listener error', () => {
    beforeEach(async () => {
      mocked(ipcRenderer.invoke).mockResolvedValueOnce(snapshot);
      mocked(listener.onSnapshot).mockImplementation(() => {
        throw new Error('listener error');
      });
      try {
        await sut.onSharedStoreChange(listener);
      } catch {
        // Ignore error.
      }
    });

    testRefusesToRegisterAnotherListener();

    testDoesNotPassPatchesToTheListener();
  });

  describe('when it is allowed to replace listeners', () => {
    const snapshot2 = {
      shouldUseDarkColors: false,
    };
    const listener2 = {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars
      onSnapshot: jest.fn((_snapshot: SharedStoreSnapshotIn) => {}),
      // eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars
      onPatch: jest.fn((_patch: IJsonPatch[]) => {}),
    };

    test('fetches a second snapshot', async () => {
      mocked(ipcRenderer.invoke).mockResolvedValueOnce(snapshot2);
      await sut.onSharedStoreChange(listener2);
      expect(ipcRenderer.invoke).toHaveBeenCalledWith(
        RendererToMainIpcMessage.GetSharedStoreSnapshot,
      );
      expect(listener2.onSnapshot).toHaveBeenCalledWith(snapshot2);
    });

    test('passes the second snapshot to the new listener', async () => {
      mocked(ipcRenderer.invoke).mockResolvedValueOnce(snapshot2);
      await sut.onSharedStoreChange(listener2);
      onSharedStorePatch(event, patch);
      expect(listener2.onPatch).toHaveBeenCalledWith(patch);
    });
  });
});
