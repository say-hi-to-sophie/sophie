/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { webFrame } from 'electron';
// eslint-disable-next-line import/no-unresolved -- Synthetic import provided by an eslint plugin.
import injectSource from 'sophie-src:@sophie/service-inject';

/**
 * Executes the service inject script in the isolated world.
 *
 * The service inject script relies on exposed APIs, so this function can only
 * be called after APIs have been exposed via `contextBridge` to the main world.
 *
 * We embed the source code of the inject script into the preload script
 * with an esbuild plugin, so there is no need to fetch it separately.
 *
 * As a tradeoff, the promise returned by `executeJavaScriptInIsolatedWorld`
 * will resolve to `unknown` (instead of rejecting) even if the injected script fails,
 * because chromium doesn't dispatch main world errors to isolated worlds.
 *
 * @return A promise that always resolves to `undefined`.
 */
async function fetchAndExecuteInjectScript(): Promise<void> {
  // Isolated world 0 is the main world.
  await webFrame.executeJavaScriptInIsolatedWorld(0, [
    {
      code: injectSource,
    },
  ]);
}

fetchAndExecuteInjectScript().catch((error) => {
  // This will never happen because of
  // https://www.electronjs.org/docs/latest/api/web-frame#webframeexecutejavascriptinisolatedworldworldid-scripts-usergesture-callback
  console.error('Failed to execute service inject:', error);
});
