import { chrome } from '../../config/buildConstants.js';
import fileUrlToDirname from '../../config/fileUrlToDirname.js';
import getEsbuildConfig from '../../config/getEsbuildConfig.js';
import srcPlugin from '../../config/srcPlugin.js';

export default getEsbuildConfig({
  absWorkingDir: fileUrlToDirname(import.meta.url),
  entryPoints: ['src/index.ts'],
  outfile: 'dist/index.cjs',
  format: 'cjs',
  platform: 'node',
  target: chrome,
  sourcemap: 'inline',
  // Absolute URL for displaying source map in the "Content scripts" tab of devtools.
  sourceRoot: 'sophie-internal://sophie/packages/service-preload/dist/',
  external: ['electron'],
  plugins: [srcPlugin],
});
