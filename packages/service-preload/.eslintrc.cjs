module.exports = {
  env: {
    node: true,
    browser: true,
  },
  rules: {
    // Using loglevel causes
    // [ERROR:network_service_instance_impl.cc(916)] Network service crashed, restarting service.
    // so we just log to the console instead.
    // See also https://github.com/electron/electron/issues/31833
    'no-console': 'off',
  },
};
