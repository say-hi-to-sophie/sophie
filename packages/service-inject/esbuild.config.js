import { chrome } from '../../config/buildConstants.js';
import fileUrlToDirname from '../../config/fileUrlToDirname.js';
import getEsbuildConfig from '../../config/getEsbuildConfig.js';

export default getEsbuildConfig({
  absWorkingDir: fileUrlToDirname(import.meta.url),
  entryPoints: ['src/index.ts'],
  outfile: 'dist/index.js',
  format: 'iife',
  platform: 'browser',
  target: chrome,
  sourcemap: 'inline',
  // Absolute URL for displaying source map in the "Page" tab of devtools.
  sourceRoot: 'sophie-internal://sophie/packages/service-inject/dist/',
});
