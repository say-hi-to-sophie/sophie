module.exports = {
  env: {
    node: false,
    browser: true,
  },
  overrides: [
    {
      files: ['.eslintrc.cjs', 'esbuild.config.js'],
      env: {
        browser: false,
        node: true,
      },
    },
  ],
};
