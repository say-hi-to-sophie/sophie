/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { test } from '@jest/globals';
import type { Global } from '@jest/types';

export interface TestIfConcurrent {
  (condition: boolean): Global.ItConcurrentBase;

  only(condition: boolean): Global.ItConcurrentBase;

  skip(condition: boolean): Global.ItConcurrentBase;
}

export interface TestIf {
  (condition: boolean): Global.ItBase;

  only(condition: boolean): Global.ItBase;

  skip(condition: boolean): Global.ItBase;

  concurrent: TestIfConcurrent;
}

const testIfConcurrent: TestIfConcurrent = function concurrent(
  condition: boolean,
) {
  return condition ? test.concurrent : test.concurrent.skip;
};

testIfConcurrent.only = function only() {
  return test.concurrent.only;
};

testIfConcurrent.skip = function skip() {
  return test.concurrent.skip;
};

const testIf: TestIf = function testIf(condition: boolean) {
  return condition ? test : test.skip;
};

testIf.only = function only() {
  return test.only;
};

testIf.skip = function skip() {
  return test.skip;
};

testIf.concurrent = testIfConcurrent;

export default testIf;
