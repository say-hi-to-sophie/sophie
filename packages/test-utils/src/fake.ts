/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type { PartialDeep } from 'type-fest';

/**
 * Creates a fake object with some properties possibly missing.
 *
 * Interacting with missing properties will cause a test failure due to
 * trying to access members of `undefined.`
 *
 * @param partialImplementation The partial fake implementation.
 * @returns The same value as `partialImplementation` but with a casted type.
 */
export default function fake<T>(partialImplementation: PartialDeep<T>): T {
  return partialImplementation as T;
}
