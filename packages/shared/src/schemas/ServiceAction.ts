/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { z } from 'zod';

import { BrowserViewBounds } from './BrowserViewBounds.js';

export const ServiceAction = /* @__PURE__ */ (() =>
  z.union([
    z.object({
      action: z.literal('set-browser-view-bounds'),
      browserViewBounds: BrowserViewBounds,
    }),
    z.object({
      action: z.literal('back'),
    }),
    z.object({
      action: z.literal('forward'),
    }),
    z.object({
      action: z.literal('reload'),
      ignoreCache: z.boolean(),
    }),
    z.object({
      action: z.literal('stop'),
    }),
    z.object({
      action: z.literal('go-home'),
    }),
    z.object({
      action: z.literal('go'),
      url: z.string(),
    }),
    z.object({
      action: z.literal('temporarily-trust-current-certificate'),
      fingerprint: z.string(),
    }),
    z.object({
      action: z.literal('open-current-url-in-external-browser'),
    }),
    z.object({
      action: z.literal('follow-popup'),
      url: z.string(),
    }),
    z.object({
      action: z.literal('open-popup-in-external-browser'),
      url: z.string(),
    }),
    z.object({
      action: z.literal('open-all-popups-in-external-browser'),
    }),
    z.object({
      action: z.literal('dismiss-popup'),
      url: z.string(),
    }),
    z.object({
      action: z.literal('dismiss-all-popups'),
    }),
    z.object({
      action: z.literal('download-certificate'),
      fingerprint: z.string().nonempty(),
    }),
  ]))();

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the schema definition.
*/
export type ServiceAction = z.infer<typeof ServiceAction>;
