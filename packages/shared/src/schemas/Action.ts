/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { z } from 'zod';

import { ServiceAction } from './ServiceAction.js';
import { ThemeSource } from './ThemeSource.js';

export const Action = /* @__PURE__ */ (() =>
  z.union([
    z.object({
      action: z.literal('set-selected-service-id'),
      serviceId: z.string(),
    }),
    z.object({
      action: z.literal('set-theme-source'),
      themeSource: ThemeSource,
    }),
    z.object({
      action: z.literal('set-show-location-bar'),
      showLocationBar: z.boolean(),
    }),
    z.object({
      action: z.literal('reload-all-services'),
    }),
    z.object({
      action: z.literal('reload-all-translations'),
    }),
    z.object({
      action: z.literal('add-missing-translation'),
      languages: z.string().nonempty().array(),
      namespace: z.string().nonempty(),
      key: z.string().nonempty(),
      value: z.string(),
    }),
    z.object({
      action: z.literal('dispatch-service-action'),
      serviceId: z.string(),
      serviceAction: ServiceAction,
    }),
  ]))();

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the schema definition.
*/
export type Action = z.infer<typeof Action>;
