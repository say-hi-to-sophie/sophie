/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

export type { default as SophieRenderer } from './contextBridge/SophieRenderer.js';

export { MainToRendererIpcMessage, RendererToMainIpcMessage } from './ipc.js';

export { Action } from './schemas/Action.js';

export { BrowserViewBounds } from './schemas/BrowserViewBounds.js';

export { ServiceAction } from './schemas/ServiceAction.js';

export { ThemeSource } from './schemas/ThemeSource.js';

export { Translation } from './schemas/Translation.js';

export type {
  CertificatePrincipal,
  CertificateSnapshotIn,
} from './stores/Certificate.js';
export { default as Certificate } from './stores/Certificate.js';

export type {
  GlobalSettingsSnapshotIn,
  GlobalSettingsSnapshotOut,
} from './stores/GlobalSettingsBase.js';
export {
  default as GlobalSettingsBase,
  defineGlobalSettingsModel,
  SYSTEM_LOCALE,
} from './stores/GlobalSettingsBase.js';

export { default as Profile } from './stores/Profile.js';

export type {
  ProfileSettingsSnapshotIn,
  ProfileSettingsSnapshotOut,
} from './stores/ProfileSettings.js';
export { default as ProfileSettings } from './stores/ProfileSettings.js';

export {
  default as ServiceBase,
  defineServiceModel,
  SecurityLabelKind,
} from './stores/ServiceBase.js';

export type {
  ServiceSettingsSnapshotIn,
  ServiceSettingsSnapshotOut,
} from './stores/ServiceSettingsBase.js';
export {
  default as ServiceSettingsBase,
  defineServiceSettingsModel,
} from './stores/ServiceSettingsBase.js';

export type { ServiceStateSnapshotIn } from './stores/ServiceState.js';
export { default as ServiceState } from './stores/ServiceState.js';

export type {
  SharedStoreListener,
  SharedStoreSnapshotIn,
  SharedStoreSnapshotOut,
} from './stores/SharedStoreBase.js';
export {
  default as SharedStoreBase,
  defineSharedStoreModel,
  FALLBACK_LOCALE,
} from './stores/SharedStoreBase.js';

export { default as WritingDirection } from './stores/WritingDirection.js';
