/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type { ResourceKey } from 'i18next';

import { Action } from '../schemas/Action.js';
import { Translation } from '../schemas/Translation.js';
import { SharedStoreListener } from '../stores/SharedStoreBase.js';

export default interface SophieRenderer {
  onSharedStoreChange(this: void, listener: SharedStoreListener): Promise<void>;

  dispatchAction(this: void, action: Action): void;

  getTranslation(this: void, translation: Translation): Promise<ResourceKey>;

  onReloadTranslations(this: void, listener: () => void): void;
}
