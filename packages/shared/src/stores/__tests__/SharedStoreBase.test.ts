/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { unprotect } from 'mobx-state-tree';

import SharedStoreBase from '../SharedStoreBase.js';

import { testProfile, testService } from './__fixtures__/serviceFixtures.js';

describe.each([true, false])(
  'when showLocationBar is %j',
  (showLocationBar) => {
    test('hides the location bar if there is no selected service', () => {
      const sharedStore = SharedStoreBase.create({
        settings: { showLocationBar },
      });

      expect(sharedStore.alwaysHideLocationBar).toBe(true);
      expect(sharedStore.alwaysShowLocationBar).toBe(false);
      expect(sharedStore.locationBarVisible).toBe(false);
      expect(sharedStore.canToggleLocationBar).toBe(false);
    });

    describe('and there is a selected service', () => {
      let sharedStore: SharedStoreBase;

      beforeEach(() => {
        sharedStore = SharedStoreBase.create({
          settings: {
            selectedService: testService.id,
            showLocationBar,
          },
          profilesById: { testProfile },
          servicesById: { testService },
        });
      });

      test('shows the location bar if requested by the user', () => {
        expect(sharedStore.alwaysHideLocationBar).toBe(false);
        expect(sharedStore.alwaysShowLocationBar).toBe(false);
        expect(sharedStore.locationBarVisible).toBe(showLocationBar);
        expect(sharedStore.canToggleLocationBar).toBe(true);
      });

      test('show the location bar if forced by the service', () => {
        unprotect(sharedStore);
        const service = sharedStore.settings.selectedService!;
        Object.defineProperty(service, 'alwaysShowLocationBar', {
          value: true,
        });

        expect(sharedStore.alwaysHideLocationBar).toBe(false);
        expect(sharedStore.alwaysShowLocationBar).toBe(true);
        expect(sharedStore.locationBarVisible).toBe(true);
        expect(sharedStore.canToggleLocationBar).toBe(false);
      });
    });
  },
);
