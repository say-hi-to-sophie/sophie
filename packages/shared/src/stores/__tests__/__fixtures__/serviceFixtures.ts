/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type { SnapshotIn } from 'mobx-state-tree';

import type Certificate from '../../Certificate.js';
import type Profile from '../../Profile.js';
import type ServiceBase from '../../ServiceBase.js';

export const testProfile: SnapshotIn<typeof Profile> = {
  id: 'testProfile',
  settings: {
    name: 'test profile',
  },
};

export const testService: SnapshotIn<typeof ServiceBase> = {
  id: 'testService',
  settings: {
    name: 'testService',
    url: 'https://example.org',
    profile: testProfile.id,
  },
};

export const testCertificate: SnapshotIn<typeof Certificate> = {
  data: '-----TEST DATA-----',
  issuer: {
    commonName: 'Test issuer',
    country: '',
    state: '',
    locality: '',
  },
  issuerName: 'Test issuer',
  subject: {
    commonName: 'Test subject',
    country: '',
    state: '',
    locality: '',
  },
  subjectName: 'Test subject',
  serialNumber: '123456',
  validStart: 1_653_551_516,
  validExpiry: 1_685_087_516,
  fingerprint: 'sha256/KUIHdVkSTzYmb2PGMotOPDm2ir52G1QyPW6rt6wUjZ0=',
};
