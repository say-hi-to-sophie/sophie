/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type { SnapshotIn } from 'mobx-state-tree';

import ServiceBase, { SecurityLabelKind } from '../ServiceBase.js';
import type { ServiceStateSnapshotIn } from '../ServiceState.js';
import SharedStoreBase from '../SharedStoreBase.js';

import {
  testCertificate,
  testProfile,
  testService,
} from './__fixtures__/serviceFixtures.js';

function createTestService(
  snapshot: Partial<SnapshotIn<typeof ServiceBase>>,
): ServiceBase {
  const sharedStore = SharedStoreBase.create({
    profilesById: {
      testProfile,
    },
    servicesById: {
      testService: {
        ...testService,
        ...snapshot,
      },
    },
  });
  return sharedStore.servicesById.get(testService.id)!;
}

test.each<{
  state: ServiceStateSnapshotIn;
  loading: boolean;
  crashed: boolean;
  hasError: boolean;
}>([
  {
    state: { type: 'initializing' },
    loading: true,
    crashed: false,
    hasError: false,
  },
  {
    state: { type: 'loading' },
    loading: true,
    crashed: false,
    hasError: false,
  },
  {
    state: { type: 'loaded' },
    loading: false,
    crashed: false,
    hasError: false,
  },
  {
    state: {
      type: 'failed',
      errorCode: 999,
      errorDesc: 'Test error',
    },
    loading: false,
    crashed: false,
    hasError: true,
  },
  {
    state: {
      type: 'certificateError',
      errorCode: 'Test certificate error',
      certificate: testCertificate,
      trust: 'pending',
    },
    loading: false,
    crashed: false,
    hasError: true,
  },
  {
    state: {
      type: 'crashed',
      reason: 'Test',
      exitCode: 255,
    },
    loading: false,
    crashed: true,
    hasError: true,
  },
])(
  'sets the loaded and error flags when the status is $state',
  ({ state, loading, crashed, hasError }) => {
    const service = createTestService({ state });

    expect(service.loading).toBe(loading);
    expect(service.crashed).toBe(crashed);
    expect(service.hasError).toBe(hasError);
    expect(service.alwaysShowLocationBar).toBe(hasError);
  },
);

test.each([
  [undefined, SecurityLabelKind.Empty, false],
  ['', SecurityLabelKind.Empty, false],
  ['https://example.org', SecurityLabelKind.SecureConnection, false],
  ['http://example.org', SecurityLabelKind.NotSecureConnection, true],
  ['https:', SecurityLabelKind.InvalidURL, true],
  ['file:///etc/shadow', SecurityLabelKind.InvalidURL, true],
  ['asdfasdfasdf', SecurityLabelKind.InvalidURL, true],
])(
  'sets the security label when the url %s is loaded',
  (
    url: string | undefined,
    labelKind: SecurityLabelKind,
    hasWarning: boolean,
  ) => {
    const service = createTestService({ currentUrl: url });

    expect(service.securityLabel).toBe(labelKind);
    expect(service.hasSecurityLabelWarning).toBe(hasWarning);
    expect(service.alwaysShowLocationBar).toBe(hasWarning);
  },
);

test('shows a certificate error warning if there is a certificate error', () => {
  const service = createTestService({
    state: {
      type: 'certificateError',
      errorCode: 'Test certificate error',
      certificate: testCertificate,
      trust: 'pending',
    },
  });

  expect(service.securityLabel).toBe(SecurityLabelKind.CertificateError);
  expect(service.hasSecurityLabelWarning).toBe(true);
  expect(service.alwaysShowLocationBar).toBe(true);
});
