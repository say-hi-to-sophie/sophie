/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { Instance, SnapshotIn, types } from 'mobx-state-tree';

import Certificate from './Certificate.js';

const ServiceState = /* @__PURE__ */ (() =>
  types.optional(
    types.union(
      types.model('ServiceInitializingState', {
        type: types.literal('initializing'),
      }),
      types.model('ServiceLoadingState', {
        type: types.literal('loading'),
      }),
      types.model('ServiceLoadedState', {
        type: types.literal('loaded'),
      }),
      types.model('ServiceFailedState', {
        type: types.literal('failed'),
        errorCode: types.integer,
        errorDesc: types.string,
      }),
      types.model('ServiceCertificateErrorState', {
        type: types.literal('certificateError'),
        errorCode: types.string,
        certificate: Certificate,
        trust: types.optional(
          types.union(
            types.literal('pending'),
            types.literal('rejected'),
            types.literal('accepted'),
          ),
          'pending',
        ),
      }),
      types.model({
        type: types.literal('crashed'),
        reason: types.string,
        exitCode: types.integer,
      }),
    ),
    { type: 'initializing' },
  ))();

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
type ServiceState = Instance<typeof ServiceState>;

export default ServiceState;

export type ServiceStateSnapshotIn = SnapshotIn<typeof ServiceState>;
