/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import {
  Instance,
  types,
  SnapshotIn,
  SnapshotOut,
  IAnyModelType,
} from 'mobx-state-tree';

import ProfileBase from './Profile.js';

export function defineServiceSettingsModel<TP extends IAnyModelType>(
  profile: TP,
) {
  return types.model('ServiceSettings', {
    name: types.string,
    profile: types.reference(profile),
    // TODO: Remove this once recipes are added.
    url: types.string,
  });
}

const ServiceSettingsBase = /* @__PURE__ */ (() =>
  defineServiceSettingsModel(ProfileBase))();

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
interface ServiceSettingsBase extends Instance<typeof ServiceSettingsBase> {}

export default ServiceSettingsBase;

export interface ServiceSettingsSnapshotIn
  extends SnapshotIn<typeof ServiceSettingsBase> {}

export interface ServiceSettingsSnapshotOut
  extends SnapshotOut<typeof ServiceSettingsBase> {}
