/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { IAnyModelType, Instance, SnapshotIn, types } from 'mobx-state-tree';

const CertificatePrincipal = /* @__PURE__ */ (() =>
  types.model('CertificatePrincipal', {
    commonName: types.string,
    organizations: types.array(types.string),
    organizationUnits: types.array(types.string),
    locality: types.string,
    state: types.string,
    country: types.string,
  }))();

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
export interface CertificatePrincipal
  extends Instance<typeof CertificatePrincipal> {}

const Certificate = /* @__PURE__ */ (() =>
  types.model('Certificate', {
    data: types.string,
    issuer: CertificatePrincipal,
    issuerName: types.string,
    issuerCert: types.maybe(types.late((): IAnyModelType => Certificate)),
    subject: CertificatePrincipal,
    subjectName: types.string,
    serialNumber: types.string,
    validStart: types.number,
    validExpiry: types.number,
    fingerprint: types.string,
  }))();

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
interface Certificate extends Instance<typeof Certificate> {}

export default Certificate;

export interface CertificateSnapshotIn extends SnapshotIn<typeof Certificate> {}
