/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { type IAnyModelType, type Instance, types } from 'mobx-state-tree';

import ServiceSettingsBase from './ServiceSettingsBase.js';
import ServiceState from './ServiceState.js';

export enum SecurityLabelKind {
  Empty = 'empty',
  SecureConnection = 'secureConnection',
  NotSecureConnection = 'notSecureConnection',
  CertificateError = 'certificateError',
  InvalidURL = 'invalidURL',
}

export function defineServiceModel<TS extends IAnyModelType>(settings: TS) {
  return types
    .model('Service', {
      id: types.identifier,
      settings,
      currentUrl: types.maybe(types.string),
      canGoBack: false,
      canGoForward: false,
      title: types.maybe(types.string),
      state: ServiceState,
      directMessageCount: 0,
      indirectMessageCount: 0,
      popups: types.array(types.string),
    })
    .views((self) => ({
      get loading(): boolean {
        return (
          self.state.type === 'initializing' || self.state.type === 'loading'
        );
      },
      get crashed(): boolean {
        return self.state.type === 'crashed';
      },
      get securityLabel(): SecurityLabelKind {
        const {
          state: { type: stateType },
          currentUrl,
        } = self;
        if (stateType === 'certificateError') {
          return SecurityLabelKind.CertificateError;
        }
        if (currentUrl === undefined || currentUrl === '') {
          return SecurityLabelKind.Empty;
        }
        try {
          const parsedUrl = new URL(currentUrl);
          switch (parsedUrl.protocol) {
            case 'https:':
              return SecurityLabelKind.SecureConnection;
            case 'http:':
              return SecurityLabelKind.NotSecureConnection;
            default:
              return SecurityLabelKind.InvalidURL;
          }
        } catch {
          return SecurityLabelKind.InvalidURL;
        }
      },
    }))
    .views((self) => ({
      get hasError(): boolean {
        return (
          self.crashed ||
          self.state.type === 'failed' ||
          self.state.type === 'certificateError'
        );
      },
      get hasSecurityLabelWarning(): boolean {
        const { securityLabel } = self;
        return (
          securityLabel !== SecurityLabelKind.Empty &&
          securityLabel !== SecurityLabelKind.SecureConnection
        );
      },
    }))
    .views((self) => ({
      get alwaysShowLocationBar(): boolean {
        return self.hasError || self.hasSecurityLabelWarning;
      },
    }));
}

const ServiceBase = /* @__PURE__ */ (() =>
  defineServiceModel(ServiceSettingsBase))();

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
interface ServiceBase extends Instance<typeof ServiceBase> {}

export default ServiceBase;
