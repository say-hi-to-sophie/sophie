/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import {
  type IJsonPatch,
  type Instance,
  types,
  type SnapshotIn,
  type SnapshotOut,
  type IAnyModelType,
} from 'mobx-state-tree';

import GlobalSettingsBase from './GlobalSettingsBase.js';
import ProfileBase from './Profile.js';
import ServiceBase from './ServiceBase.js';
import WritingDirection from './WritingDirection.js';

export const FALLBACK_LOCALE = 'en';

export function defineSharedStoreModel<
  TG extends IAnyModelType,
  TP extends IAnyModelType,
  TS extends IAnyModelType,
>(globalSettings: TG, profile: TP, service: TS) {
  return types
    .model('SharedStore', {
      settings: types.optional(globalSettings, {}),
      profilesById: types.map(profile),
      profiles: types.array(types.reference(profile)),
      servicesById: types.map(service),
      services: types.array(types.reference(service)),
      shouldUseDarkColors: false,
      language: FALLBACK_LOCALE,
      writingDirection: types.optional(WritingDirection, 'ltr'),
    })
    .views((self) => ({
      get alwaysHideLocationBar(): boolean {
        const settings = self.settings as GlobalSettingsBase;
        return settings.selectedService === undefined;
      },
      get alwaysShowLocationBar(): boolean {
        const settings = self.settings as GlobalSettingsBase;
        const selectedService = settings.selectedService as
          | ServiceBase
          | undefined;
        return selectedService?.alwaysShowLocationBar ?? false;
      },
    }))
    .views((self) => ({
      get locationBarVisible(): boolean {
        const settings = self.settings as GlobalSettingsBase;
        return (
          !self.alwaysHideLocationBar &&
          (self.alwaysShowLocationBar || settings.showLocationBar)
        );
      },
      get canToggleLocationBar(): boolean {
        return !(self.alwaysHideLocationBar || self.alwaysShowLocationBar);
      },
    }));
}

const SharedStoreBase = /* @__PURE__ */ (() =>
  defineSharedStoreModel(GlobalSettingsBase, ProfileBase, ServiceBase))();

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
interface SharedStoreBase extends Instance<typeof SharedStoreBase> {}

export default SharedStoreBase;

export interface SharedStoreSnapshotIn
  extends SnapshotIn<typeof SharedStoreBase> {}

export interface SharedStoreSnapshotOut
  extends SnapshotOut<typeof SharedStoreBase> {}

export interface SharedStoreListener {
  onSnapshot(snapshot: SharedStoreSnapshotIn): void;

  onPatch(patches: IJsonPatch[]): void;
}
