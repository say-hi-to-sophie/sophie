module.exports = {
  env: {
    node: true,
    browser: false,
  },
  rules: {
    // This is an application, so we're allowed to `exit` from it.
    'unicorn/no-process-exit': 'off',
  },
};
