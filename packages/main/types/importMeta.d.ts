interface ImportMeta {
  env: {
    DEV: boolean;
    MODE: string;
    PROD: boolean;
    VITE_DEV_SERVER_URL?: string | undefined;
    GIT_SHA: string;
    GIT_BRANCH: string;
    BUILD_DATE: number;
    SUPPORTED_LOCALES: string[];
  };
}
