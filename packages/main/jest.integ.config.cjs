const path = require('node:path');

const baseConfig = require('../../config/jest.config.base.cjs');

/** @type {import('@jest/types').Config.InitialOptions} */
module.exports = {
  ...baseConfig,
  rootDir: 'src',
  globalSetup: path.join(__dirname, '../../config/electronJestGlobalSetup.cjs'),
  globalTeardown: path.join(
    __dirname,
    '../../config/electronJestGlobalTeardown.cjs',
  ),
  testEnvironment: path.join(
    __dirname,
    '../../config/electronJestEnvironment.cjs',
  ),
  setupFilesAfterEnv: [
    path.join(__dirname, '../../config/electronJestSetup.cjs'),
  ],
};
