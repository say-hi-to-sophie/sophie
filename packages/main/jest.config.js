import baseConfig from '../../config/jest.config.base.js';

export default {
  ...baseConfig,
  rootDir: 'src',
};
