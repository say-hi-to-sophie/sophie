/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

// eslint-disable-next-line unicorn/import-style -- Import the type `ChalkInstance` separately.
import chalk, { ChalkInstance } from 'chalk';
import loglevel from 'loglevel';
import prefix from 'loglevel-plugin-prefix';

switch (import.meta.env.MODE) {
  case 'development':
    loglevel.setLevel('debug', false);
    break;
  case 'test':
    loglevel.setLevel('silent', false);
    break;
  default:
    loglevel.setLevel('info', false);
    break;
}

const COLORS: Map<string, ChalkInstance> = new Map([
  ['TRACE', chalk.magenta],
  ['DEBUG', chalk.cyan],
  ['INFO', chalk.blue],
  ['WARN', chalk.yellow],
  ['ERROR', chalk.red],
  ['CRITICAL', chalk.red],
]);

prefix.reg(loglevel);
prefix.apply(loglevel, {
  format(level, name, timestamp) {
    const levelColor = COLORS.get(level) ?? chalk.gray;
    const timeStr = timestamp.toString();
    const nameStr =
      name === undefined ? levelColor(':') : ` ${chalk.green(`${name}:`)}`;
    return `${chalk.gray(`[${timeStr}]`)} ${levelColor(level)}${nameStr}`;
  },
});

export default function getLogger(loggerName: string): loglevel.Logger {
  return loglevel.getLogger(loggerName);
}
