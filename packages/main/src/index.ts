/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 * Copyright (C)  2022 Vijay A <vraravam@users.noreply.github.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import os from 'node:os';

import { app } from 'electron';

import { enableStacktraceSourceMaps } from './infrastructure/electron/impl/devTools.js';
import electronShell from './infrastructure/electron/impl/electronShell.js';
import initReactions from './initReactions.js';
import MainStore from './stores/MainStore.js';
import getLogger from './utils/getLogger.js';

const isDevelopment = import.meta.env.MODE === 'development';

const log = getLogger('index');

// Always enable sandboxing.
app.enableSandbox();

if (isDevelopment) {
  enableStacktraceSourceMaps();
}

// Only allow a single instance at a time.
const isSingleInstance = app.requestSingleInstanceLock();
if (!isSingleInstance) {
  app.quit();
  process.exit(0);
}

// Disable chromium's MPRIS integration, which is usually more annoying
// (triggered by random sounds played by websites) than useful.
app.commandLine.appendSwitch(
  'disable-features',
  'HardwareMediaKeyHandling,MediaSessionService',
);

app.setAboutPanelOptions({
  applicationVersion: [
    `Version: ${app.getVersion()}`,
    `Electron: ${process.versions.electron}`,
    `Chrome: ${process.versions.chrome}`,
    `Node.js: ${process.versions.node}`,
    `Platform: ${os.platform()} ${os.release()}`,
    `Arch: ${os.arch()}`,
    `Build date: ${new Date(
      Number(import.meta.env.BUILD_DATE),
    ).toLocaleString()}`,
    `Git SHA: ${import.meta.env.GIT_SHA}`,
    `Git branch: ${import.meta.env.GIT_BRANCH}`,
  ].join('\n'),
  version: '',
});

const store = MainStore.create({}, electronShell);

app.on('second-instance', () => {
  store.mainWindow?.bringToForeground();
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

const isMac = os.platform() === 'darwin';

initReactions(store, isDevelopment, isMac)
  // eslint-disable-next-line promise/always-return -- `then` instead of top-level await.
  .then((disposeCompositionRoot) => {
    app.on('will-quit', disposeCompositionRoot);
  })
  .catch((error) => {
    log.log('Failed to initialize application', error);
  });
