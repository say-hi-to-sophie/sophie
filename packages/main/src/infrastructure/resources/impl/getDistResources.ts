/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import path from 'node:path';
import { pathToFileURL, URL } from 'node:url';

import Resources from '../Resources.js';

export default function getDistResources(
  devMode: boolean,
  /*
    eslint-disable-next-line unicorn/prefer-module --
    Electron apps run in a commonjs environment, so there is no `import.meta.url`.
  */
  thisDir = __dirname,
  devServerURL = import.meta.env?.VITE_DEV_SERVER_URL,
): Resources {
  const packagesRoot = path.join(thisDir, '..', '..');
  const localizationRoot = path.join(packagesRoot, '..', 'locales');

  function getPath(packageName: string, relativePathInPackage: string): string {
    return path.join(packagesRoot, packageName, 'dist', relativePathInPackage);
  }

  function getFileURL(
    packageName: string,
    relativePathInPackage: string,
  ): string {
    const absolutePath = getPath(packageName, relativePathInPackage);
    return pathToFileURL(absolutePath).toString();
  }

  return {
    getPath,
    getLocalizationPath(language, fileName) {
      return path.join(localizationRoot, language, fileName);
    },
    getFileURL,
    getRendererURL:
      devMode && devServerURL !== undefined
        ? (relativePathInRendererPackage) =>
            new URL(relativePathInRendererPackage, devServerURL).toString()
        : (relativePathInRendererPackage) =>
            getFileURL('renderer', relativePathInRendererPackage),
  };
}
