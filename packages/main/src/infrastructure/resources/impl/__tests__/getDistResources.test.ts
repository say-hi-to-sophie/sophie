/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import os from 'node:os';

import Resources from '../../Resources.js';
import getDistResources from '../getDistResources.js';

const defaultDevServerURL = 'http://localhost:3000/';

const [
  thisDir,
  preloadIndexPath,
  preloadIndexFileURL,
  rendererIndexFileURL,
  rendererRootFileURL,
] =
  os.platform() === 'win32'
    ? [
        'C:\\Program Files\\sophie\\resources\\app.asar\\packages\\main\\dist',
        'C:\\Program Files\\sophie\\resources\\app.asar\\packages\\preload\\dist\\index.cjs',
        'file:///C:/Program Files/sophie/resources/app.asar/packages/preload/dist/index.cjs',
        'file:///C:/Program Files/sophie/resources/app.asar/packages/renderer/dist/index.html',
        'file:///C:/Program Files/sophie/resources/app.asar/packages/renderer/dist/',
      ]
    : [
        '/opt/sophie/resources/app.asar/packages/main/dist',
        '/opt/sophie/resources/app.asar/packages/preload/dist/index.cjs',
        'file:///opt/sophie/resources/app.asar/packages/preload/dist/index.cjs',
        'file:///opt/sophie/resources/app.asar/packages/renderer/dist/index.html',
        'file:///opt/sophie/resources/app.asar/packages/renderer/dist/',
      ];

const fileURLs: [string, string] = [rendererIndexFileURL, rendererRootFileURL];

describe.each([
  ['not in dev mode', false, undefined, ...fileURLs],
  [
    'not in dev mode with VITE_DEV_SERVER_URL set',
    false,
    defaultDevServerURL,
    ...fileURLs,
  ],
  ['in dev mode with no VITE_DEV_SERVER_URL', true, undefined, ...fileURLs],
  [
    'in dev mode with VITE_DEV_SERVER_URL set',
    true,
    defaultDevServerURL,
    `${defaultDevServerURL}index.html`,
    defaultDevServerURL,
  ],
])(
  'when %s',
  (
    _description: string,
    devMode: boolean,
    devServerURL: string | undefined,
    rendererIndexURL: string,
    rendererRootURL: string,
  ) => {
    let resources: Resources;

    beforeEach(() => {
      resources = getDistResources(devMode, thisDir, devServerURL);
    });

    test('getPath returns the path to the requested resource', () => {
      const path = resources.getPath('preload', 'index.cjs');
      expect(path).toBe(preloadIndexPath);
    });

    test('getFileURL returns the file URL to the requested resource', () => {
      const url = resources.getFileURL('preload', 'index.cjs');
      expect(url).toBe(preloadIndexFileURL);
    });

    describe('getRendererURL', () => {
      test('returns the URL to the requested resource', () => {
        const url = resources.getRendererURL('index.html');
        expect(url).toBe(rendererIndexURL);
      });

      test('returns the root URL', () => {
        const url = resources.getRendererURL('/');
        expect(url).toBe(rendererRootURL);
      });
    });
  },
);
