/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type MainStore from '../../stores/MainStore.js';
import type Profile from '../../stores/Profile.js';
import type Service from '../../stores/Service.js';

export interface ViewFactory {
  createMainWindow(store: MainStore): Promise<MainWindow>;

  createPartition(profile: Profile): Partition;

  createServiceView(service: Service, partition: Partition): ServiceView;

  dispose(): void;
}

export interface MainWindow {
  bringToForeground(): void;

  setServiceView(serviceView: ServiceView | undefined): void;

  reloadTranslations(): void;

  dispose(): void;
}

export interface Partition {
  readonly id: string;

  dispose(): void;
}

export interface ServiceView {
  readonly id: string;

  readonly partitionId: string;

  loadURL(url: string): void;

  goBack(): void;

  goForward(): void;

  reload(ignoreCache: boolean): void;

  stop(): void;

  toggleDeveloperTools(): void;

  updateBounds(): void;

  dispose(): void;
}
