/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import UserAgents from '../UserAgents.js';

let userAgents: UserAgents;

beforeEach(() => {
  userAgents = new UserAgents(
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) sophie/0.1.0 Chrome/102.0.5005.27 Electron/19.0.0-beta.4 Safari/537.36',
  );
});

test('removes Electron from the user agent outside dev mode', () => {
  expect(userAgents.fallbackUserAgent(false)).toBe(
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.27 Safari/537.36',
  );
});

test('does not remove Electron from the user agent in dev mode', () => {
  expect(userAgents.fallbackUserAgent(true)).toBe(
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) sophie/0.1.0 Chrome/102.0.5005.27 Electron/19.0.0-beta.4 Safari/537.36',
  );
});

test('displays the Chrome version for services', () => {
  expect(userAgents.serviceUserAgent('https://example.org')).toBe(
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.27 Safari/537.36',
  );
});

test('does not display the Chrome version when loggin in to Google', () => {
  expect(
    userAgents.serviceUserAgent(
      'https://accounts.google.com/signin/v2/identifier',
    ),
  ).toBe(
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Safari/537.36',
  );
});
