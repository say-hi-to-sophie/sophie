/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/* eslint-disable no-param-reassign -- Actions modify the `self` parameter. */

import { jest } from '@jest/globals';
import { configure } from 'mobx';
import {
  destroy,
  flow,
  type IJsonPatch,
  type Instance,
  unprotect,
} from 'mobx-state-tree';

import MainStore from '../../../stores/MainStore.js';
import RendererBridge, { type PatchListener } from '../RendererBridge.js';

const TestStore = MainStore.actions((theSelf) => ({
  /**
   * Dispatch `action` as an action in the context of the store.
   *
   * We cannot use `action` from `'mobx'` here, because it is not bound to
   * a `'mobx-state-tree'` store, so it won't trigger our middleware.
   *
   * @param action The action to dispatch.
   */
  testAction(action: (self: MainStore) => void): void {
    action(theSelf);
  },
  /**
   * Executes a flow in the context of the store.
   */
  testFlow: flow(function* testFlow(
    action: (self: MainStore) => Generator<PromiseLike<void>, void, void>,
  ) {
    yield* action(theSelf);
  }),
}));

/*
  eslint-disable-next-line @typescript-eslint/no-empty-interface,
  @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
type TestStore = Instance<typeof TestStore>;

function resolveImmediately(): Promise<void> {
  return new Promise((resolve) => {
    setImmediate(resolve);
  });
}

function rejectImmediately(error: Error): Promise<void> {
  return new Promise((_resolve, reject) => {
    setImmediate(() => reject(error));
  });
}

const shouldUseDarkColorsPatch: IJsonPatch[] = [
  {
    op: 'replace',
    path: '/shouldUseDarkColors',
    value: false,
  },
];

let store: TestStore;
let listener: PatchListener;
let bridge: RendererBridge;

beforeEach(() => {
  store = TestStore.create({
    shared: {
      shouldUseDarkColors: true,
    },
  });
  listener = jest.fn<PatchListener>();
  bridge = new RendererBridge(store, listener);
});

afterEach(() => {
  bridge.dispose();
  destroy(store);
});

test('sets the initial snapshot', () => {
  expect(bridge.snapshot.shouldUseDarkColors).toBe(true);
});

test('sets the snapshot after an action', () => {
  store.shared.setShouldUseDarkColors(false);
  expect(bridge.snapshot.shouldUseDarkColors).toBe(false);
});

test('generates a patch after an action', () => {
  store.shared.setShouldUseDarkColors(false);
  expect(listener).toHaveBeenCalledTimes(1);
  expect(listener).toHaveBeenCalledWith(shouldUseDarkColorsPatch);
});

describe('when the store is unprotected', () => {
  beforeAll(() => configure({ enforceActions: 'never' }));

  beforeEach(() => unprotect(store));

  afterAll(() => configure({ enforceActions: 'observed' }));

  test('sets the snapshot after directly setting a value', () => {
    store.shared.shouldUseDarkColors = false;
    expect(bridge.snapshot.shouldUseDarkColors).toBe(false);
  });

  test('generates a patch after directly setting a value', () => {
    store.shared.shouldUseDarkColors = false;
    expect(listener).toHaveBeenCalledTimes(1);
    expect(listener).toHaveBeenCalledWith(shouldUseDarkColorsPatch);
  });
});

test('does not generate a patch if an action does not modify the store', () => {
  store.testAction(() => {});
  expect(listener).not.toHaveBeenCalled();
});

test('does not set the snapshot until the end of the action', () => {
  store.testAction(({ shared }) => {
    shared.shouldUseDarkColors = false;
    expect(bridge.snapshot.shouldUseDarkColors).not.toBe(false);
  });
});

test('does not generate a patch until the end of the action', () => {
  store.testAction(({ shared }) => {
    shared.shouldUseDarkColors = false;
    expect(listener).not.toHaveBeenCalled();
  });
});

test('does not set the snapshot until the end of the action with nested action', () => {
  store.testAction(({ shared }) => {
    shared.setShouldUseDarkColors(false);
    expect(bridge.snapshot.shouldUseDarkColors).not.toBe(false);
  });
});

test('does not generate a patch until the end of the action with nested action', () => {
  store.testAction(({ shared }) => {
    shared.setShouldUseDarkColors(false);
    expect(listener).not.toHaveBeenCalled();
  });
});

test('generates a single patch for multiple nested actions', () => {
  store.testAction(({ shared }) => {
    shared.setShouldUseDarkColors(false);
    shared.setLanguage('testLanguage', 'rtl');
  });
  expect(listener).toHaveBeenCalledTimes(1);
  expect(listener).toHaveBeenCalledWith([
    ...shouldUseDarkColorsPatch,
    {
      op: 'replace',
      path: '/language',
      value: 'testLanguage',
    },
    {
      op: 'replace',
      path: '/writingDirection',
      value: 'rtl',
    },
  ]);
});

test('does not abort on a failing action', () => {
  expect(() =>
    store.testAction(({ shared }) => {
      shared.setShouldUseDarkColors(false);
      throw new Error('test error');
    }),
  ).toThrow('test error');
  expect(bridge.snapshot.shouldUseDarkColors).toBe(false);
  expect(listener).toHaveBeenCalledTimes(1);
  expect(listener).toHaveBeenCalledWith(shouldUseDarkColorsPatch);
});

test('generates a snapshot and a patch in the beginning of a flow', async () => {
  await store.testFlow(function* successfulTestFlowBeginning({ shared }) {
    shared.setShouldUseDarkColors(false);

    expect(bridge.snapshot.shouldUseDarkColors).not.toBe(false);
    expect(listener).not.toHaveBeenCalled();

    yield resolveImmediately();

    expect(bridge.snapshot.shouldUseDarkColors).toBe(false);
    expect(listener).toHaveBeenCalledTimes(1);
    expect(listener).toHaveBeenCalledWith(shouldUseDarkColorsPatch);
  });
});

test('generates a snapshot and a patch in the middle of a flow', async () => {
  await store.testFlow(function* successfulTestFlowMiddle({ shared }) {
    yield resolveImmediately();

    shared.setShouldUseDarkColors(false);

    expect(bridge.snapshot.shouldUseDarkColors).not.toBe(false);
    expect(listener).not.toHaveBeenCalled();

    yield resolveImmediately();

    expect(bridge.snapshot.shouldUseDarkColors).toBe(false);
    expect(listener).toHaveBeenCalledTimes(1);
    expect(listener).toHaveBeenCalledWith(shouldUseDarkColorsPatch);
  });
});

test('generates a snapshot and a patch in the end of a flow', async () => {
  await store.testFlow(function* successfulTestFlowEnd({ shared }) {
    yield resolveImmediately();

    shared.setShouldUseDarkColors(false);

    expect(bridge.snapshot.shouldUseDarkColors).not.toBe(false);
    expect(listener).not.toHaveBeenCalled();
  });
  expect(bridge.snapshot.shouldUseDarkColors).toBe(false);
  expect(listener).toHaveBeenCalledTimes(1);
  expect(listener).toHaveBeenCalledWith(shouldUseDarkColorsPatch);
});

test('generates a snapshot and a patch when a flow fails synchronously in the beginning', async () => {
  await expect(
    // eslint-disable-next-line require-yield -- We simulate a failure before `yield`.
    store.testFlow(function* synchronousFailureTestFlowBeginning({ shared }) {
      shared.setShouldUseDarkColors(false);
      throw new Error('test error');
    }),
  ).rejects.toBeInstanceOf(Error);
  expect(bridge.snapshot.shouldUseDarkColors).toBe(false);
  expect(listener).toHaveBeenCalledTimes(1);
  expect(listener).toHaveBeenCalledWith(shouldUseDarkColorsPatch);
});

test('generates a snapshot and a patch when a flow fails asynchronously in the beginning', async () => {
  await expect(
    store.testFlow(function* asynchronousFailureTestFlowBeginning({ shared }) {
      shared.setShouldUseDarkColors(false);
      yield rejectImmediately(new Error('test error'));
    }),
  ).rejects.toBeInstanceOf(Error);
  expect(bridge.snapshot.shouldUseDarkColors).toBe(false);
  expect(listener).toHaveBeenCalledTimes(1);
  expect(listener).toHaveBeenCalledWith(shouldUseDarkColorsPatch);
});

test('generates a snapshot and a patch when a flow fails synchronously in the middle', async () => {
  await expect(
    store.testFlow(function* synchronousFailureTestFlowMiddle({ shared }) {
      yield resolveImmediately();
      shared.setShouldUseDarkColors(false);
      throw new Error('test error');
    }),
  ).rejects.toBeInstanceOf(Error);
  expect(bridge.snapshot.shouldUseDarkColors).toBe(false);
  expect(listener).toHaveBeenCalledTimes(1);
  expect(listener).toHaveBeenCalledWith(shouldUseDarkColorsPatch);
});

test('generates a snapshot and a patch when a flow fails asynchronously in the middle', async () => {
  await expect(
    store.testFlow(function* asynchronousFailureTestFlowMiddle({ shared }) {
      yield resolveImmediately();
      shared.setShouldUseDarkColors(false);
      yield rejectImmediately(new Error('test error'));
    }),
  ).rejects.toBeInstanceOf(Error);
  expect(bridge.snapshot.shouldUseDarkColors).toBe(false);
  expect(listener).toHaveBeenCalledTimes(1);
  expect(listener).toHaveBeenCalledWith(shouldUseDarkColorsPatch);
});
