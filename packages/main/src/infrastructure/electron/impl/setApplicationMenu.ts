/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { Menu, MenuItemConstructorOptions } from 'electron';
import { autorun } from 'mobx';
import { addDisposer } from 'mobx-state-tree';

import type MainStore from '../../../stores/MainStore.js';

export default function setApplicationMenu(
  store: MainStore,
  devMode: boolean,
  isMac: boolean,
): void {
  const dispose = autorun(() => {
    const translation = store.useTranslation();
    if (!translation.ready) {
      return;
    }
    const { t } = translation;

    const { settings, shared, visibleService } = store;
    const { selectedService } = settings;
    const {
      canSwitchServices,
      canToggleLocationBar,
      locationBarVisible,
      services,
    } = shared;

    const template: MenuItemConstructorOptions[] = [
      ...(isMac ? ([{ role: 'appMenu' }] as MenuItemConstructorOptions[]) : []),
      { role: 'fileMenu' },
      { role: 'editMenu' },
      {
        role: 'viewMenu',
        submenu: [
          {
            label: t<string>('menu.view.showLocationBar'),
            accelerator: 'CommandOrControl+Shift+L',
            type: 'checkbox',
            checked: locationBarVisible,
            enabled: canToggleLocationBar,
            click() {
              settings.toggleLocationBar();
            },
          },
          { type: 'separator' },
          {
            label: t<string>('menu.view.reload'),
            accelerator: 'CommandOrControl+R',
            enabled: selectedService !== undefined,
            click() {
              selectedService?.reload(false);
            },
          },
          {
            label: t<string>('menu.view.forceReload'),
            accelerator: 'CommandOrControl+Shift+R',
            enabled: selectedService !== undefined,
            click() {
              selectedService?.reload(true);
            },
          },
          {
            label: t<string>('menu.view.toggleDeveloperTools'),
            accelerator: 'CommandOrControl+Shift+I',
            enabled: visibleService !== undefined,
            click() {
              visibleService?.toggleDeveloperTools();
            },
          },
          { type: 'separator' },
          ...(devMode
            ? ([
                {
                  role: 'forceReload',
                  label: t<string>('menu.view.reloadSophie'),
                  accelerator: 'CommandOrControl+Shift+Alt+R',
                },
                {
                  role: 'toggleDevTools',
                  label: t<string>('menu.view.toggleSophieDeveloperTools'),
                  accelerator: 'CommandOrControl+Shift+Alt+I',
                },
                { type: 'separator' },
              ] as MenuItemConstructorOptions[])
            : []),
          { role: 'togglefullscreen' },
        ],
      },
      {
        label: t<string>('menu.servicesMenu'),
        submenu: [
          {
            label: t<string>('menu.services.nextService'),
            accelerator: 'CommandOrControl+Tab',
            enabled: canSwitchServices,
            click() {
              shared.activateNextService();
            },
          },
          {
            label: t<string>('menu.services.previousService'),
            accelerator: 'CommandOrControl+Shift+Tab',
            enabled: canSwitchServices,
            click() {
              shared.activatePreviousService();
            },
          },
          ...(services.length > 0
            ? ([
                { type: 'separator' },
                ...services.map(
                  (service, index): MenuItemConstructorOptions => ({
                    label: service.config.name,
                    type: 'radio',
                    ...(index < 9
                      ? {
                          accelerator: `CommandOrControl+${index + 1}`,
                        }
                      : {}),
                    checked: selectedService === service,
                    click() {
                      settings.setSelectedService(service);
                    },
                  }),
                ),
              ] as MenuItemConstructorOptions[])
            : []),
        ],
      },
      { role: 'windowMenu' },
      {
        role: 'help',
        submenu: [
          {
            label: t<string>('menu.help.gitlab'),
            click() {
              store.openWebpageInBrowser();
            },
          },
          ...(isMac
            ? []
            : ([
                {
                  role: 'about',
                  click() {
                    store.openAboutDialog();
                  },
                },
              ] as MenuItemConstructorOptions[])),
        ],
      },
    ];
    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);
  });
  addDisposer(store, dispose);
}
