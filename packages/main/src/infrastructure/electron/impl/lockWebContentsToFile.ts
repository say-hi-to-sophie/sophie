/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type { WebContents } from 'electron';

import getLogger from '../../../utils/getLogger.js';
import isErrno from '../../../utils/isErrno.js';
import type Resources from '../../resources/Resources.js';

const log = getLogger('lockWebContentsToFile');

/**
 * Loads the specified file in the webContents and prevent navigating away.
 *
 * Both navigating away to a different URL and opening a new window will be disallowed.
 *
 * @param resources The resource handle associated with the paths and URL of the application.
 * @param filePath The path to the file in the render package to load.
 * @param webContents The webContents to lock.
 * @returns A promise that resolves when the webpage is loaded.
 */
export default async function lockWebContentsToFile(
  resources: Resources,
  filePath: string,
  webContents: WebContents,
): Promise<void> {
  const pageURL = resources.getRendererURL(filePath);

  webContents.setWindowOpenHandler(() => ({ action: 'deny' }));

  webContents.on('will-navigate', (event, url) => {
    if (url !== pageURL) {
      log.error(
        'Prevented webContents locked to',
        pageURL,
        'from navigating to',
        url,
      );
      event.preventDefault();
    }
  });

  try {
    await webContents.loadURL(pageURL);
  } catch (error) {
    // Chromium will throw `ERR_ABORTED` when the vite dev server is still initializing,
    // but will load the page nevertheless.
    if (!isErrno(error, 'ERR_ABORTED') || !pageURL.startsWith('http:')) {
      throw error;
    }
  }
}
