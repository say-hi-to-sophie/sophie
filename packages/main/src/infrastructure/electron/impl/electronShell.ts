/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { writeFile } from 'node:fs/promises';

import { app, dialog, shell } from 'electron';
import { getLogger } from 'loglevel';

import type MainEnv from '../../../stores/MainEnv.js';

const log = getLogger('ElectronShell');

const electronShell: MainEnv = {
  openURLInExternalBrowser(url: string): void {
    shell.openExternal(url).catch((error) => {
      log.error('Failed to open', url, 'in external program', error);
    });
  },
  openAboutDialog(): void {
    app.showAboutPanel();
  },
  async saveTextFile(filename: string, value: string): Promise<void> {
    const result = await dialog.showSaveDialog({
      defaultPath: filename,
    });
    if (result.canceled || result.filePath === undefined) {
      log.debug('Saving file', filename, 'canceled');
      return;
    }
    await writeFile(result.filePath, value, 'utf8');
    log.debug('Saved file', filename, 'to', result.filePath);
  },
};

export default electronShell;
