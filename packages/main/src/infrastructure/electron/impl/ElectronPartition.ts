/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { Session, session } from 'electron';

import type Profile from '../../../stores/Profile.js';
import type { Partition } from '../types.js';

import type ElectronViewFactory from './ElectronViewFactory.js';

export default class ElectronPartition implements Partition {
  readonly id: string;

  readonly session: Session;

  constructor(profile: Profile, parent: ElectronViewFactory) {
    this.id = profile.id;
    this.session = session.fromPartition(`persist:${profile.id}`);
    this.session.setPermissionRequestHandler(
      (_webContents, permission, callback) => {
        // TODO Handle screen sharing.
        callback(permission === 'notifications');
      },
    );
    this.session.setUserAgent(parent.userAgents.serviceUserAgent());
    this.session.webRequest.onBeforeSendHeaders(
      ({ url, requestHeaders }, callback) => {
        callback({
          requestHeaders: {
            ...requestHeaders,
            'User-Agent': parent.userAgents.serviceUserAgent(url),
          },
        });
      },
    );
  }

  // eslint-disable-next-line class-methods-use-this -- Implementing interface method.
  dispose(): void {
    // No reactions to dispose yet.
  }
}
