/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { URL } from 'node:url';

import type { Session } from 'electron';

import getLogger from '../../../utils/getLogger.js';
import type Resources from '../../resources/Resources.js';

/**
 * URL prefixes Sophie is allowed load in dev mode.
 *
 * In dev mode, in addition to the application itself,
 * Sophie must be able do download and load the devtools and related extensions,
 * so we have to make exceptions in the UI process request filter.
 */
export const DEVMODE_ALLOWED_URL_PREFIXES = [
  'chrome-extension:',
  'devtools:',
  'https://clients2.google.com/service/update2/crx',
  'https://clients2.googleusercontent.com/crx',
];

const log = getLogger('hardenSession');

/**
 * Hardens a session to prevent loading resources outside the renderer resources and
 * to reject all permission requests.
 *
 * In dev mode, installation of extensions and opening the devtools will be allowed.
 *
 * @param resources The resource handle associated with the paths and URL of the application.
 * @param devMode Whether the application is in development mode.
 * @param session The session to harden.
 */
export default function hardenSession(
  resources: Resources,
  devMode: boolean,
  session: Session,
): void {
  session.setPermissionRequestHandler((_webContents, _permission, callback) => {
    callback(false);
  });

  const rendererBaseURL = resources.getRendererURL('/');
  log.debug('Renderer base URL:', rendererBaseURL);

  const allowedPrefixes = [rendererBaseURL];
  if (devMode) {
    const webSocketBaseURL = rendererBaseURL.replace(/^http(s)?:/, 'ws$1:');
    log.debug('WebSocket base URL:', webSocketBaseURL);
    allowedPrefixes.push(webSocketBaseURL, ...DEVMODE_ALLOWED_URL_PREFIXES);
  }

  function shouldCancelRequest(url: string, method: string): boolean {
    if (method !== 'GET') {
      return true;
    }
    let normalizedURL: string;
    try {
      normalizedURL = new URL(url).toString();
    } catch {
      return true;
    }
    return !allowedPrefixes.some((prefix) => normalizedURL.startsWith(prefix));
  }

  session.webRequest.onBeforeRequest(({ url, method }, callback) => {
    const cancel = shouldCancelRequest(url, method);
    if (cancel) {
      log.error('Prevented loading', method, url);
    }
    callback({ cancel });
  });
}
