/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type { BrowserWindow } from 'electron';

/* eslint-disable
  import/no-extraneous-dependencies,
  global-require,
  @typescript-eslint/no-var-requires,
  unicorn/prefer-module --
  Hack to lazily require a CJS module from an ES module transpiled into a CJS module.
*/

/**
 * Enables using source maps for node stack traces.
 */
export function enableStacktraceSourceMaps(): void {
  const sourceMapSupport =
    require('source-map-support') as typeof import('source-map-support');
  sourceMapSupport.install();
}

/**
 * Installs the react and redux developer tools extensions.
 *
 * We use the redux devtools and connect the mobx store to it with `mst-middlewares`,
 * because the mobx-state-tree devtools are currently unmaintained.
 */
export async function installDevToolsExtensions(): Promise<void> {
  const {
    default: installExtension,
    REACT_DEVELOPER_TOOLS,
    REDUX_DEVTOOLS,
  } = require('electron-devtools-installer') as typeof import('electron-devtools-installer');
  await installExtension([REACT_DEVELOPER_TOOLS, REDUX_DEVTOOLS], {
    forceDownload: false,
    loadExtensionOptions: {
      allowFileAccess: true,
    },
  });
}

/**
 * Opens the developer tools while applying a workaround to enable the redux devtools.
 *
 * @param browserWindow The browser window to open the devtools in.
 * @see https://github.com/MarshallOfSound/electron-devtools-installer/issues/195#issuecomment-998872878
 */
export function openDevToolsWhenReady(browserWindow: BrowserWindow): void {
  const { webContents } = browserWindow;
  webContents.once('dom-ready', () => {
    webContents.once('devtools-opened', () => {
      browserWindow?.focus();
    });
    webContents.openDevTools();
  });
}
