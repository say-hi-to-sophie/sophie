/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type MainStore from '../../../stores/MainStore.js';
import type Profile from '../../../stores/Profile.js';
import type Service from '../../../stores/Service.js';
import type Resources from '../../resources/Resources.js';
import type UserAgents from '../UserAgents.js';
import type {
  MainWindow,
  Partition,
  ServiceView,
  ViewFactory,
} from '../types.js';

import ElectronMainWindow from './ElectronMainWindow.js';
import ElectronPartition from './ElectronPartition.js';
import ElectronServiceView from './ElectronServiceView.js';

export default class ElectronViewFactory implements ViewFactory {
  private readonly webContentsIdToServiceView = new Map<
    number,
    ElectronServiceView
  >();

  constructor(
    readonly userAgents: UserAgents,
    readonly resources: Resources,
    readonly devMode: boolean,
  ) {}

  async createMainWindow(store: MainStore): Promise<MainWindow> {
    const mainWindow = new ElectronMainWindow(store, this);
    await mainWindow.loadInterface();
    return mainWindow;
  }

  createPartition(profile: Profile): Partition {
    return new ElectronPartition(profile, this);
  }

  createServiceView(service: Service, partition: Partition): ServiceView {
    if (partition instanceof ElectronPartition) {
      const serviceView = new ElectronServiceView(
        service,
        this.resources,
        partition,
        this,
      );
      this.webContentsIdToServiceView.set(
        serviceView.webContentsId,
        serviceView,
      );
      return serviceView;
    }
    throw new TypeError('Unexpected ProfileSession is not a WrappedSession');
  }

  dispose(): void {
    if (this.webContentsIdToServiceView.size > 0) {
      throw new Error(
        'Must dispose all ServiceView instances before disposing ViewFactory',
      );
    }
  }

  unregisterServiceView(id: number): void {
    this.webContentsIdToServiceView.delete(id);
  }
}
