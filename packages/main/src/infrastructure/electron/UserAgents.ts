/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

const CHROMELESS_USER_AGENT_REGEX = /^[^:]+:\/\/accounts\.google\.[^./]+\//;

export default class UserAgents {
  private readonly default: string;

  private readonly chromeless: string;

  constructor(readonly mainWindowUserAgent: string) {
    this.default = mainWindowUserAgent.replaceAll(
      /\s(sophie|Electron)\/\S+/g,
      '',
    );
    this.chromeless = this.default.replace(/ Chrome\/\S+/, '');
  }

  serviceUserAgent(url?: string | undefined): string {
    return url !== undefined && CHROMELESS_USER_AGENT_REGEX.test(url)
      ? this.chromeless
      : this.default;
  }

  fallbackUserAgent(devMode: boolean): string {
    // Removing the electron version breaks redux devtools, so we only do this in production.
    return devMode ? this.mainWindowUserAgent : this.default;
  }
}
