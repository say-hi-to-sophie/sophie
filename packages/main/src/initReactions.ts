/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { app, session } from 'electron';

import LocalizationFiles from './i18n/impl/LocaltizationFiles.js';
import loadLocalization from './i18n/loadLocalization.js';
import ConfigFile from './infrastructure/config/impl/ConfigFile.js';
import UserAgents from './infrastructure/electron/UserAgents.js';
import ElectronViewFactory from './infrastructure/electron/impl/ElectronViewFactory.js';
import { installDevToolsExtensions } from './infrastructure/electron/impl/devTools.js';
import hardenSession from './infrastructure/electron/impl/hardenSession.js';
import setApplicationMenu from './infrastructure/electron/impl/setApplicationMenu.js';
import getDistResources from './infrastructure/resources/impl/getDistResources.js';
import loadServices from './reactions/loadServices.js';
import synchronizeConfig from './reactions/synchronizeConfig.js';
import synchronizeNativeTheme from './reactions/synchronizeNativeTheme.js';
import type MainStore from './stores/MainStore.js';
import type Disposer from './utils/Disposer.js';

export default async function initReactions(
  store: MainStore,
  devMode: boolean,
  isMac: boolean,
): Promise<Disposer> {
  const configRepository = new ConfigFile(app.getPath('userData'));
  const disposeConfigController = await synchronizeConfig(
    store.shared,
    configRepository,
  );
  const resources = getDistResources(devMode);
  // Ideally, we would the the chromium `--lang` according to the settings store here,
  // but `app.isReady()` is often already `true`, so we're too late to do that.
  await app.whenReady();
  const localizationFiles = new LocalizationFiles(resources);
  const localizationLoaded = loadLocalization(
    store,
    app.getLocale(),
    import.meta.env.SUPPORTED_LOCALES,
    localizationFiles,
    devMode,
  );
  const disposeNativeThemeController = synchronizeNativeTheme(store.shared);
  hardenSession(resources, devMode, session.defaultSession);
  const userAgents = new UserAgents(app.userAgentFallback);
  app.userAgentFallback = userAgents.fallbackUserAgent(devMode);
  const devToolsLoaded = devMode
    ? installDevToolsExtensions()
    : Promise.resolve();
  const viewFactory = new ElectronViewFactory(userAgents, resources, devMode);
  const mainWindow = (async () => {
    await localizationLoaded;
    setApplicationMenu(store, devMode, isMac);
    await devToolsLoaded;
    return viewFactory.createMainWindow(store);
  })();
  loadServices(store, viewFactory);
  store.setMainWindow(await mainWindow);
  return () => {
    disposeNativeThemeController();
    disposeConfigController();
  };
}
