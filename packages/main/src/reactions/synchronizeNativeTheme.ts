/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { nativeTheme } from 'electron';
import { autorun } from 'mobx';

import type SharedStore from '../stores/SharedStore.js';
import type Disposer from '../utils/Disposer.js';
import getLogger from '../utils/getLogger.js';

const log = getLogger('synchronizeNativeTheme');

export default function initNativeTheme(store: SharedStore): Disposer {
  const disposeThemeSourceReaction = autorun(() => {
    nativeTheme.themeSource = store.settings.themeSource;
    log.debug('Set theme source:', store.settings.themeSource);
  });

  store.setShouldUseDarkColors(nativeTheme.shouldUseDarkColors);
  const shouldUseDarkColorsListener = () => {
    store.setShouldUseDarkColors(nativeTheme.shouldUseDarkColors);
    log.debug('Set should use dark colors:', nativeTheme.shouldUseDarkColors);
  };
  nativeTheme.on('updated', shouldUseDarkColorsListener);

  return () => {
    nativeTheme.off('updated', shouldUseDarkColorsListener);
    disposeThemeSourceReaction();
  };
}
