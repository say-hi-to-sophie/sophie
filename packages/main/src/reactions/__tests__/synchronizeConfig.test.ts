/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { jest } from '@jest/globals';
import { mocked } from 'jest-mock';

import type ConfigRepository from '../../infrastructure/config/ConfigRepository.js';
import SharedStore from '../../stores/SharedStore.js';
import type Disposer from '../../utils/Disposer.js';
import synchronizeConfig, { serializeConfig } from '../synchronizeConfig.js';

let store: SharedStore;
const repository: ConfigRepository = {
  readConfig: jest.fn<ConfigRepository['readConfig']>(),
  writeConfig: jest.fn<ConfigRepository['writeConfig']>(),
  watchConfig: jest.fn<ConfigRepository['watchConfig']>(),
};
const lessThanThrottleMs = 100;
const throttleMs = 1000;

beforeAll(() => {
  jest.useFakeTimers();
});

beforeEach(() => {
  store = SharedStore.create();
});

describe('when initializing', () => {
  describe('when there is no config file', () => {
    beforeEach(() => {
      mocked(repository.readConfig).mockResolvedValueOnce({
        found: false,
      });
    });

    test('should create a new config file', async () => {
      await synchronizeConfig(store, repository);
      expect(repository.writeConfig).toHaveBeenCalledTimes(1);
    });

    test('should bail if there is an an error creating the config file', async () => {
      mocked(repository.writeConfig).mockRejectedValue(new Error('boo'));
      await expect(() =>
        synchronizeConfig(store, repository),
      ).rejects.toBeInstanceOf(Error);
    });
  });

  describe('when there is a valid config file', () => {
    beforeEach(() => {
      mocked(repository.readConfig).mockResolvedValueOnce({
        found: true,
        contents: serializeConfig({
          // Use a default empty config file to not trigger config rewrite.
          ...store.config,
          themeSource: 'dark',
        }),
      });
    });

    test('should read the existing config file is there is one', async () => {
      await synchronizeConfig(store, repository);
      expect(repository.writeConfig).not.toHaveBeenCalled();
      expect(store.settings.themeSource).toBe('dark');
    });

    test('should bail if it cannot set up a watcher', async () => {
      mocked(repository.watchConfig).mockImplementationOnce(() => {
        throw new Error('boo');
      });
      await expect(() =>
        synchronizeConfig(store, repository),
      ).rejects.toBeInstanceOf(Error);
    });
  });

  test('should update the config file if new details are added during read', async () => {
    mocked(repository.readConfig).mockResolvedValueOnce({
      found: true,
      contents: `{
  "themeSource": "light",
  "profiles": [
    {
      "name": "Test profile"
    }
  ]
}
`,
    });
    await synchronizeConfig(store, repository);
    expect(repository.writeConfig).toHaveBeenCalledTimes(1);
  });

  test('should not apply an invalid config file but should not overwrite it', async () => {
    mocked(repository.readConfig).mockResolvedValueOnce({
      found: true,
      contents: `{
  "themeSource": -1
}
`,
    });
    await synchronizeConfig(store, repository);
    expect(store.settings.themeSource).not.toBe(-1);
    expect(repository.writeConfig).not.toHaveBeenCalled();
  });

  test('should bail if it cannot determine whether there is a config file', async () => {
    mocked(repository.readConfig).mockRejectedValue(new Error('boo'));
    await expect(() =>
      synchronizeConfig(store, repository),
    ).rejects.toBeInstanceOf(Error);
  });
});

describe('when it has loaded the config', () => {
  let sutDisposer: Disposer;
  const watcherDisposer: Disposer = jest.fn();
  let configChangedCallback: () => Promise<void>;

  beforeEach(async () => {
    mocked(repository.readConfig).mockResolvedValueOnce({
      found: true,
      contents: serializeConfig(store.config),
    });
    mocked(repository.watchConfig).mockReturnValueOnce(watcherDisposer);
    sutDisposer = await synchronizeConfig(store, repository, throttleMs);
    [[configChangedCallback]] = mocked(repository.watchConfig).mock.calls;
    jest.resetAllMocks();
  });

  test('should throttle saving changes to the config file', () => {
    mocked(repository.writeConfig).mockResolvedValue();
    store.settings.setThemeSource('dark');
    jest.advanceTimersByTime(lessThanThrottleMs);
    store.settings.setThemeSource('light');
    jest.advanceTimersByTime(throttleMs);
    expect(repository.writeConfig).toHaveBeenCalledTimes(1);
  });

  test('should handle config writing errors gracefully', () => {
    mocked(repository.writeConfig).mockRejectedValue(new Error('boo'));
    store.settings.setThemeSource('dark');
    jest.advanceTimersByTime(throttleMs);
    expect(repository.writeConfig).toHaveBeenCalledTimes(1);
  });

  test('should read the config file when it has changed', async () => {
    mocked(repository.readConfig).mockResolvedValueOnce({
      found: true,
      contents: serializeConfig({
        // Use a default empty config file to not trigger config rewrite.
        ...store.config,
        themeSource: 'dark',
      }),
    });
    await configChangedCallback();
    // Do not write back the changes we have just read.
    expect(repository.writeConfig).not.toHaveBeenCalled();
    expect(store.settings.themeSource).toBe('dark');
  });

  test('should update the config file if new details are added', async () => {
    mocked(repository.readConfig).mockResolvedValueOnce({
      found: true,
      contents: `{
  "themeSource": "light",
  "profiles": [
    {
      "name": "Test profile"
    }
  ]
}
`,
    });
    await configChangedCallback();
    expect(repository.writeConfig).toHaveBeenCalledTimes(1);
  });

  test('should not apply an invalid config file when it has changed but should not overwrite it', async () => {
    mocked(repository.readConfig).mockResolvedValueOnce({
      found: true,
      contents: `{
  "themeSource": -1
}
`,
    });
    await configChangedCallback();
    expect(store.settings.themeSource).not.toBe(-1);
    expect(repository.writeConfig).not.toHaveBeenCalled();
  });

  test('should handle config reading errors gracefully', async () => {
    mocked(repository.readConfig).mockRejectedValue(new Error('boo'));
    await expect(configChangedCallback()).resolves.not.toThrow();
  });

  describe('when it was disposed', () => {
    beforeEach(() => {
      sutDisposer();
    });

    test('should dispose the watcher', () => {
      expect(watcherDisposer).toHaveBeenCalled();
    });

    test('should not listen to store changes any more', () => {
      store.settings.setThemeSource('dark');
      jest.advanceTimersByTime(2 * throttleMs);
      expect(repository.writeConfig).not.toHaveBeenCalled();
    });
  });
});
