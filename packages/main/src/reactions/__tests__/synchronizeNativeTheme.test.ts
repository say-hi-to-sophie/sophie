/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { jest } from '@jest/globals';
import { mocked } from 'jest-mock';

import SharedStore from '../../stores/SharedStore.js';
import type Disposer from '../../utils/Disposer.js';

let shouldUseDarkColors = false;

jest.unstable_mockModule('electron', () => ({
  nativeTheme: {
    themeSource: 'system',
    get shouldUseDarkColors() {
      return shouldUseDarkColors;
    },
    on: jest.fn(),
    off: jest.fn(),
  },
}));

const { nativeTheme } = await import('electron');
const { default: synchronizeNativeTheme } = await import(
  '../synchronizeNativeTheme.js'
);

let store: SharedStore;
let disposeSut: Disposer;

beforeEach(() => {
  store = SharedStore.create();
  disposeSut = synchronizeNativeTheme(store);
});

test('should register a nativeTheme updated listener', () => {
  expect(nativeTheme.on).toHaveBeenCalledWith('updated', expect.anything());
});

test('should synchronize themeSource changes to the nativeTheme', () => {
  store.settings.setThemeSource('dark');
  expect(nativeTheme.themeSource).toBe('dark');
});

test('should synchronize shouldUseDarkColors changes to the store', () => {
  const listener = mocked(nativeTheme.on).mock.calls.find(
    ([event]) => event === 'updated',
  )![1];
  shouldUseDarkColors = true;
  listener();
  expect(store.shouldUseDarkColors).toBe(true);
});

test('should remove the listener on dispose', () => {
  const listener = mocked(nativeTheme.on).mock.calls.find(
    ([event]) => event === 'updated',
  )![1];
  disposeSut();
  expect(nativeTheme.off).toHaveBeenCalledWith('updated', listener);
});
