/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type { Action } from '@sophie/shared';
import type { ResourceKey } from 'i18next';
import { flow, Instance, types } from 'mobx-state-tree';

import type I18nStore from '../i18n/I18nStore.js';
import type { UseTranslationResult } from '../i18n/I18nStore.js';
import type { MainWindow } from '../infrastructure/electron/types.js';
import getLogger from '../utils/getLogger.js';

import GlobalSettings from './GlobalSettings.js';
import { getEnv } from './MainEnv.js';
import Profile from './Profile.js';
import Service from './Service.js';
import SharedStore from './SharedStore.js';

const log = getLogger('MainStore');

const MainStore = types
  .model('MainStore', {
    shared: types.optional(SharedStore, {}),
    i18n: types.frozen<I18nStore | undefined>(),
  })
  .views((self) => ({
    get settings(): GlobalSettings {
      return self.shared.settings;
    },
    get profiles(): Profile[] {
      return self.shared.profiles;
    },
    get services(): Service[] {
      return self.shared.services;
    },
    get visibleService(): Service | undefined {
      const { selectedService } = this.settings;
      return selectedService !== undefined && selectedService.shouldBeVisible
        ? selectedService
        : undefined;
    },
    useTranslation(ns?: string): UseTranslationResult {
      return self.i18n?.useTranslation(ns) ?? { ready: false };
    },
    getTranslation(language: string, namespace: string): Promise<ResourceKey> {
      if (self.i18n === undefined) {
        throw new Error('i18next has not been set');
      }
      return self.i18n.getTranslation(language, namespace);
    },
  }))
  .volatile(
    (): {
      mainWindow: MainWindow | undefined;
    } => ({
      mainWindow: undefined,
    }),
  )
  .actions((self) => ({
    setMainWindow(mainWindow: MainWindow | undefined): void {
      self.mainWindow = mainWindow;
    },
    openWebpageInBrowser() {
      getEnv(self).openURLInExternalBrowser(
        'https://gitlab.com/say-hi-to-sophie/shophie',
      );
    },
    openAboutDialog() {
      getEnv(self).openAboutDialog();
    },
    beforeDestroy(): void {
      self.mainWindow?.dispose();
    },
    setI18n(i18n: I18nStore): void {
      self.i18n = i18n;
    },
    addMissingTranslation(
      languages: string[],
      namespace: string,
      key: string,
      value: string,
    ): void {
      self.i18n?.addMissingTranslation(languages, namespace, key, value);
    },
    reloadTranslations: flow(function* reloadTranslations() {
      if (self.i18n !== undefined) {
        yield self.i18n.reloadTranslations();
        self.mainWindow?.reloadTranslations();
      }
    }),
  }))
  .actions((self) => ({
    dispatch(action: Action): void {
      switch (action.action) {
        case 'set-selected-service-id':
          self.settings.setSelectedServiceId(action.serviceId);
          break;
        case 'set-theme-source':
          self.settings.setThemeSource(action.themeSource);
          break;
        case 'set-show-location-bar':
          self.settings.setShowLocationBar(action.showLocationBar);
          break;
        case 'reload-all-services':
          self.services.forEach((service) => service.reload());
          break;
        case 'reload-all-translations':
          if (self.i18n !== undefined) {
            self.reloadTranslations().catch((error) => {
              log.error('Failed to reload translations', error);
            });
          }
          break;
        case 'add-missing-translation':
          self.addMissingTranslation(
            action.languages,
            action.namespace,
            action.key,
            action.value,
          );
          break;
        case 'dispatch-service-action': {
          const { serviceId, serviceAction } = action;
          const service = self.shared.servicesById.get(serviceId);
          if (service === undefined) {
            log.error(
              'No such service',
              serviceId,
              'to dispatch action',
              serviceAction,
            );
          } else {
            service.dispatch(serviceAction);
          }
          break;
        }
        default:
          log.error('Unknown action to dispatch', action);
          break;
      }
    },
  }));

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
interface MainStore extends Instance<typeof MainStore> {}

export default MainStore;
