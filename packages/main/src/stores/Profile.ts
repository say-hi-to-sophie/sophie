/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import {
  type Certificate,
  type CertificateSnapshotIn,
  Profile as ProfileBase,
} from '@sophie/shared';
import { getSnapshot, type Instance } from 'mobx-state-tree';

import type ProfileConfig from './config/ProfileConfig.js';

const Profile = ProfileBase.volatile(
  (): {
    temporarilyTrustedCertificates: string[];
  } => ({
    temporarilyTrustedCertificates: [],
  }),
)
  .views((self) => ({
    get config(): ProfileConfig {
      const { id, settings } = self;
      return { ...getSnapshot(settings), id };
    },
    isCertificateTemporarilyTrusted(
      certificate: CertificateSnapshotIn,
    ): boolean {
      return self.temporarilyTrustedCertificates.includes(
        certificate.fingerprint,
      );
    },
  }))
  .actions((self) => ({
    temporarilyTrustCertificate(certificate: Certificate): void {
      self.temporarilyTrustedCertificates.push(certificate.fingerprint);
    },
  }));

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
interface Profile extends Instance<typeof Profile> {}

export default Profile;
