/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import SharedStore from '../SharedStore.js';
import type Config from '../config/Config.js';
import type ProfileConfig from '../config/ProfileConfig.js';
import type ServiceConfig from '../config/ServiceConfig.js';

const profileProps: ProfileConfig = {
  name: 'Test profile',
};

const serviceProps: ServiceConfig = {
  name: 'Test service',
  url: 'https://example.com',
};

let sut: SharedStore;

beforeEach(() => {
  sut = SharedStore.create();
});

describe('loadConfig', () => {
  test('loads profiles with an ID', () => {
    sut.loadConfig({
      profiles: [
        {
          id: 'someId',
          ...profileProps,
        },
      ],
    });
    expect(sut.profiles[0].id).toBe('someId');
  });

  test('generates an ID for profiles without and ID', () => {
    sut.loadConfig({
      profiles: [profileProps],
    });
    expect(sut.profiles[0].id).toBeDefined();
  });

  test('loads services with an ID and a profile', () => {
    sut.loadConfig({
      profiles: [
        {
          id: 'someProfileId',
          ...profileProps,
        },
      ],
      services: [
        {
          id: 'someServiceId',
          profile: 'someProfileId',
          ...serviceProps,
        },
      ],
    });
    expect(sut.services[0].id).toBe('someServiceId');
    expect(sut.services[0].settings.profile).toBe(sut.profiles[0]);
  });

  test('refuses to load a profile without a name', () => {
    expect(() => {
      sut.loadConfig({
        profiles: [
          {
            id: 'someProfileId',
            ...profileProps,
            name: undefined,
          },
        ],
      } as unknown as Config);
    }).toThrow();
    expect(sut.profiles).toHaveLength(0);
  });

  test('loads services without an ID but with a profile', () => {
    sut.loadConfig({
      profiles: [
        {
          id: 'someProfileId',
          ...profileProps,
        },
      ],
      services: [
        {
          profile: 'someProfileId',
          ...serviceProps,
        },
      ],
    });
    expect(sut.services[0].id).toBeDefined();
    expect(sut.services[0].settings.profile).toBe(sut.profiles[0]);
  });

  test('creates a profile for a service with an ID but no profile', () => {
    sut.loadConfig({
      services: [
        {
          id: 'someServiceId',
          ...serviceProps,
        },
      ],
    });
    expect(sut.services[0].id).toBe('someServiceId');
    expect(sut.services[0].settings.profile).toBeDefined();
    expect(sut.services[0].settings.profile.settings.name).toBe(
      serviceProps.name,
    );
  });

  test('creates a profile for a service without an ID or profile', () => {
    sut.loadConfig({
      services: [
        {
          ...serviceProps,
        },
      ],
    });
    expect(sut.services[0].id).toBeDefined();
    expect(sut.services[0].settings.profile).toBeDefined();
    expect(sut.services[0].settings.profile.settings.name).toBe(
      serviceProps.name,
    );
  });

  test('refuses to load a service without a name', () => {
    expect(() => {
      sut.loadConfig({
        services: [
          {
            id: 'someServiceId',
            ...serviceProps,
            name: undefined,
          },
        ],
      } as unknown as Config);
    }).toThrow();
    expect(sut.profiles).toHaveLength(0);
    expect(sut.services).toHaveLength(0);
  });
});
