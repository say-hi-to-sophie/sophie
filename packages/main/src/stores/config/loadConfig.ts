/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { ProfileSettingsSnapshotIn } from '@sophie/shared';
import {
  applySnapshot,
  IMSTArray,
  IMSTMap,
  IReferenceType,
  IStateTreeNode,
  IType,
} from 'mobx-state-tree';
import { nanoid } from 'nanoid';
import slug from 'slug';

import GlobalSettings from '../GlobalSettings.js';
import type Profile from '../Profile.js';
import type Service from '../Service.js';
import type { ServiceSettingsSnapshotIn } from '../ServiceSettings.js';

import type Config from './Config.js';
import type ProfileConfig from './ProfileConfig.js';
import type ServiceConfig from './ServiceConfig.js';

function generateId(name?: string | undefined): string {
  const nameSlug = name === undefined ? '' : slug(name);
  return `${nameSlug}_${nanoid()}`;
}

function addMissingProfileIds(
  profileConfigs: ProfileConfig[] | undefined,
): [string, ProfileSettingsSnapshotIn][] {
  return (profileConfigs ?? []).map((profileConfig) => {
    const { id, ...settings } = profileConfig;
    return [id === undefined ? generateId(settings.name) : id, settings];
  });
}

function addMissingServiceIdsAndProfiles(
  serviceConfigs: ServiceConfig[] | undefined,
  profiles: [string, ProfileSettingsSnapshotIn][],
): [string, ServiceSettingsSnapshotIn][] {
  return (serviceConfigs ?? []).map((serviceConfig) => {
    const { id, ...settings } = serviceConfig;
    const { name } = settings;
    let { profile: profileId } = settings;
    if (profileId === undefined) {
      profileId = generateId(name);
      profiles.push([profileId, { name }]);
    }
    return [
      id === undefined ? generateId(name) : id,
      { ...settings, profile: profileId },
    ];
  });
}

type TypeWithSettings<C> = IType<
  { id: string; settings: C },
  unknown,
  { settings: IStateTreeNode<IType<C, unknown, unknown>> }
>;

function applySettings<C, D extends TypeWithSettings<C>>(
  current: IMSTArray<IReferenceType<D>>,
  currentById: IMSTMap<D>,
  toApply: [string, C][],
): void {
  const toApplyById = new Map(toApply);
  const toDelete = new Set(currentById.keys());
  toApplyById.forEach((settingsSnapshot, id) => {
    const model = currentById.get(id);
    if (model === undefined) {
      currentById.set(id, {
        id,
        settings: settingsSnapshot,
      });
    } else {
      toDelete.delete(id);
      applySnapshot(model.settings, settingsSnapshot);
    }
  });
  toDelete.forEach((id) => currentById.delete(id));
  current.clear();
  current.push(...toApply.map(([id]) => id));
}

export default function loadConfig(
  target: {
    readonly profiles: IMSTArray<IReferenceType<typeof Profile>>;
    readonly profilesById: IMSTMap<typeof Profile>;
    readonly services: IMSTArray<IReferenceType<typeof Service>>;
    readonly servicesById: IMSTMap<typeof Service>;
    readonly settings: GlobalSettings;
  },
  config: Config,
): void {
  const { profiles, profilesById, services, servicesById, settings } = target;
  const {
    profiles: profilesConfig,
    services: servicesConfig,
    ...settingsToApply
  } = config;
  const profilesToApply = addMissingProfileIds(profilesConfig);
  const servicesToApply = addMissingServiceIdsAndProfiles(
    servicesConfig,
    profilesToApply,
  );
  applySettings(profiles, profilesById, profilesToApply);
  applySettings(services, servicesById, servicesToApply);
  const { selectedService } = settingsToApply;
  // Be more robust against when a deleted service is selected.
  if (
    typeof selectedService !== 'string' ||
    !servicesById.has(selectedService)
  ) {
    settingsToApply.selectedService = undefined;
  }
  applySnapshot(settings, settingsToApply);
  if (settings.selectedService === undefined && services.length > 0) {
    [settings.selectedService] = services;
  }
}
