/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type { LoggerModule } from 'i18next';

import getLogger from '../utils/getLogger.js';

const log = getLogger('i18nLog');

/*
  eslint-disable-next-line @typescript-eslint/unbound-method --
  loglevel log methods don't rely on `this`, so this is safe,
  and also keeps stacktraces smaller.
*/
const { debug, warn, error } = log;

const i18nLog: LoggerModule = {
  type: 'logger',
  log: debug,
  warn,
  error,
};

export default i18nLog;
