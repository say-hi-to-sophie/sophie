/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { readFile, writeFile } from 'node:fs/promises';

import type { ResourceKey } from 'i18next';

import type Resources from '../../infrastructure/resources/Resources.js';
import getLogger from '../../utils/getLogger.js';
import isErrno from '../../utils/isErrno.js';
import type LocatlizationRepository from '../LocalizationRepository.js';

const log = getLogger('LocalizationFiles');

export default class LocalizationFiles implements LocatlizationRepository {
  constructor(private readonly resources: Resources) {}

  async getResourceContents(
    language: string,
    namespace: string,
  ): Promise<ResourceKey> {
    const fileName = this.resources.getLocalizationPath(
      language,
      `${namespace}.json`,
    );
    const contents = await readFile(fileName, 'utf8');
    log.info('Read localization file', fileName);
    // The contents of the file should come from a signed archive during production,
    // so we don't need to (and can't) validate them.
    return JSON.parse(contents) as ResourceKey;
  }

  private getMissingLocalizationPath(
    language: string,
    namespace: string,
  ): string {
    return this.resources.getLocalizationPath(
      language,
      `${namespace}.missing.json`,
    );
  }

  async getMissingResourceContents(
    language: string,
    namespace: string,
  ): Promise<ResourceKey> {
    const fileName = this.getMissingLocalizationPath(language, namespace);
    try {
      const contents = await readFile(fileName, 'utf8');
      // The contents of the file are only used during development,
      // so we don't need to (and can't) validate them.
      return JSON.parse(contents) as ResourceKey;
    } catch (error) {
      if (isErrno(error, 'ENOENT')) {
        log.debug(
          'No missing translations file',
          language,
          'for namespace',
          namespace,
        );
        return {};
      }
      throw error;
    }
  }

  setMissingResourceContents(
    language: string,
    namespace: string,
    data: ResourceKey,
  ): Promise<void> {
    const fileName = this.getMissingLocalizationPath(language, namespace);
    const contents = JSON.stringify(data, undefined, 2);
    return writeFile(fileName, contents, 'utf8');
  }
}
