import { readdir } from 'node:fs/promises';
import path from 'node:path';

import getRepoInfo from 'git-repo-info';

import { node } from '../../config/buildConstants.js';
import fileUrlToDirname from '../../config/fileUrlToDirname.js';
import getEsbuildConfig from '../../config/getEsbuildConfig.js';

/** @type {string} */
const thisDir = fileUrlToDirname(import.meta.url);

const externalPackages = ['electron'];

if (process.env.MODE !== 'development') {
  externalPackages.push('electron-devtools-installer', 'source-map-support');
}

const gitInfo = getRepoInfo();

/** @type {string[]} */
const locales = await readdir(path.join(thisDir, '../../locales'));

export default getEsbuildConfig(
  {
    absWorkingDir: thisDir,
    entryPoints: ['src/index.ts'],
    outfile: 'dist/index.cjs',
    format: 'cjs',
    platform: 'node',
    target: node,
    external: externalPackages,
  },
  {
    VITE_DEV_SERVER_URL: process.env.VITE_DEV_SERVER_URL || undefined,
    GIT_SHA: gitInfo.abbreviatedSha,
    GIT_BRANCH: gitInfo.branch,
    BUILD_DATE: Date.now(),
    SUPPORTED_LOCALES: locales,
  },
);
