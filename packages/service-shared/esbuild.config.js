import { chrome, node } from '../../config/buildConstants.js';
import fileUrlToDirname from '../../config/fileUrlToDirname.js';
import getEsbuildConfig from '../../config/getEsbuildConfig.js';

export default getEsbuildConfig({
  absWorkingDir: fileUrlToDirname(import.meta.url),
  entryPoints: ['src/index.ts'],
  outfile: 'dist/index.mjs',
  format: 'esm',
  // The package that includes this one will have a header comment,
  // no need to have an additional one here.
  banner: {},
  // Keep @__PURE__ annotations intact.
  minify: false,
  platform: 'node',
  target: [chrome, node],
  external: ['zod'],
});
