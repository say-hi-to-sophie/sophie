module.exports = {
  env: {
    // We must run in both node and browser, so we can't depend on either of them.
    node: false,
    browser: false,
  },
  overrides: [
    {
      files: ['.eslintrc.cjs', 'esbuild.config.js'],
      env: {
        node: true,
      },
    },
  ],
};
