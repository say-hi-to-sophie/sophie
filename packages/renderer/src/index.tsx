/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import CssBaseline from '@mui/material/CssBaseline';
import { autorun } from 'mobx';
import { addDisposer } from 'mobx-state-tree';
import React, { Suspense, lazy } from 'react';
import { createRoot } from 'react-dom/client';

import Loading from './components/Loading.js';
import ThemeProvider from './components/ThemeProvider.js';
import { exposeToReduxDevtools, hotReload } from './devTools.js';
import RtlCacheProvider from './i18n/RtlCacheProvider.js';
import createRtlCache from './i18n/createRtlCache.js';
import loadRendererLocalization from './i18n/loadRendererLoalization.js';
import { createAndConnectRendererStore } from './stores/RendererStore.js';
import { getLogger } from './utils/log.js';

const isDevelopment = import.meta.env.MODE === 'development';

const log = getLogger('index');

if (isDevelopment) {
  hotReload();
}

const { sophieRenderer: ipc } = window;

const store = await createAndConnectRendererStore(ipc);

if (isDevelopment) {
  exposeToReduxDevtools(store).catch((error) => {
    log.error('Cannot initialize redux devtools', error);
  });
}

loadRendererLocalization(store, ipc, isDevelopment);

const disposeSetHtmlLang = autorun(() => {
  const {
    shared: { language, writingDirection },
  } = store;
  document.documentElement.lang = language;
  document.documentElement.dir = writingDirection;
});
addDisposer(store, disposeSetHtmlLang);

const rtlCache = createRtlCache();

const App = lazy(() => import('./components/App.js'));

function Root(): JSX.Element {
  return (
    <React.StrictMode>
      <RtlCacheProvider store={store} rtlCache={rtlCache}>
        <ThemeProvider store={store}>
          <CssBaseline enableColorScheme />
          <Suspense fallback={<Loading />}>
            <App store={store} devMode={isDevelopment} />
          </Suspense>
        </ThemeProvider>
      </RtlCacheProvider>
    </React.StrictMode>
  );
}

const rootElement = document.querySelector('#app');
if (rootElement === null) {
  log.error('Root element not found');
} else {
  const root = createRoot(rootElement);
  root.render(<Root />);
}
