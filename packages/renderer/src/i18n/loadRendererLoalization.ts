/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { FALLBACK_LOCALE, SophieRenderer } from '@sophie/shared';
import i18next from 'i18next';
import { reaction } from 'mobx';
import { addDisposer } from 'mobx-state-tree';
import { initReactI18next } from 'react-i18next';

import RendererStore from '../stores/RendererStore.js';
import { getLogger } from '../utils/log.js';

import RendererIpcI18nBackend from './RendererIpcI18nBackend.js';

const log = getLogger('loadRendererLocalization');

export default function loadRendererLocalization(
  store: RendererStore,
  ipc: SophieRenderer,
  devMode: boolean,
): void {
  const loadAsync = async () => {
    const i18n = i18next
      .createInstance({
        lng: store.shared.language,
        fallbackLng: [FALLBACK_LOCALE],
        interpolation: {
          escapeValue: false, // Not needed for react
        },
        debug: devMode,
        saveMissing: devMode,
      })
      .use(new RendererIpcI18nBackend(ipc, devMode))
      .use(initReactI18next);

    if (devMode) {
      const reloadTranslationsAsync = async () => {
        await i18n.reloadResources();
        if (i18n.isInitialized) {
          // Spuriously change language to re-trigger `useTranslation` hooks.
          await i18n.changeLanguage(store.shared.language);
        }
        log.info('Reloaded translations');
      };

      ipc.onReloadTranslations(() => {
        reloadTranslationsAsync().catch((error) => {
          log.error('Failed to reload translations', error);
        });
      });
    }

    await i18n.init();

    const disposeChangeLanguage = reaction(
      () => store.shared.language,
      (languageToSet) => {
        if (i18n.language !== languageToSet) {
          i18n.changeLanguage(languageToSet).catch((error) => {
            log.error('Failed to change language', error);
          });
        }
      },
      {
        fireImmediately: true,
      },
    );
    addDisposer(store, disposeChangeLanguage);
  };

  loadAsync().catch((error) => {
    log.error('Failed to connect to load localization', error);
  });
}
