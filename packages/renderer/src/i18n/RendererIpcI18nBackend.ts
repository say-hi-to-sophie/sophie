/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { SophieRenderer } from '@sophie/shared';
import type { BackendModule, ReadCallback } from 'i18next';

export default class RendererIpcI18nBackend implements BackendModule<unknown> {
  type = 'backend' as const;

  constructor(
    private readonly ipc: SophieRenderer,
    private readonly devMode = false,
  ) {}

  // eslint-disable-next-line class-methods-use-this -- Method required by interface.
  init() {}

  read(language: string, namespace: string, callback: ReadCallback): void {
    const readAsync = async () => {
      const translations = await this.ipc.getTranslation({
        language,
        namespace,
      });
      // eslint-disable-next-line unicorn/no-null -- `i18next` API requires `null`.
      setTimeout(() => callback(null, translations), 0);
    };

    readAsync().catch((error) => {
      const callbackError =
        error instanceof Error
          ? error
          : new Error(`Unknown error: ${JSON.stringify(error)}`);
      /*
        eslint-disable-next-line promise/no-callback-in-promise, unicorn/no-null --
        Converting from promise based API to a callback. `i18next` API requires `null`.
      */
      setTimeout(() => callback(callbackError, null), 0);
    });
  }

  create(
    languages: string[],
    namespace: string,
    key: string,
    fallbackValue: string,
  ): void {
    if (!this.devMode) {
      throw new Error('Refusing to add missing translation in production mode');
    }
    this.ipc.dispatchAction({
      action: 'add-missing-translation',
      languages,
      namespace,
      key,
      value: fallbackValue,
    });
  }
}
