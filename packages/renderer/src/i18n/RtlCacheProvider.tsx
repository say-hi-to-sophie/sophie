/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type { EmotionCache } from '@emotion/cache';
import { CacheProvider } from '@emotion/react';
import { observer } from 'mobx-react-lite';
import React, { type ReactNode } from 'react';

import type RendererStore from '../stores/RendererStore.js';

function RtlCacheProvider({
  children,
  store: {
    shared: { writingDirection },
  },
  rtlCache,
}: {
  children?: ReactNode;
  store: RendererStore;
  rtlCache: EmotionCache;
}): JSX.Element {
  return writingDirection === 'rtl' ? (
    <CacheProvider value={rtlCache}>{children}</CacheProvider>
  ) : (
    /*
      eslint-disable-next-line react/jsx-no-useless-fragment --
      Wrap expression in a fragment to satisfy typescript.
    */
    <>{children}</>
  );
}

RtlCacheProvider.defaultProps = {
  children: undefined,
};

export default observer(RtlCacheProvider);
