/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { defineSharedStoreModel } from '@sophie/shared';
import { Instance } from 'mobx-state-tree';

import GlobalSettings from './GlobalSettings.js';
import Profile from './Profile.js';
import Service from './Service.js';

const SharedStore = defineSharedStoreModel(
  GlobalSettings,
  Profile,
  Service,
).actions((self) => ({
  toggleDarkMode(): void {
    if (self.shouldUseDarkColors) {
      self.settings.setThemeSource('light');
    } else {
      self.settings.setThemeSource('dark');
    }
  },
}));

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
interface SharedStore extends Instance<typeof SharedStore> {}

export default SharedStore;

export type {
  SharedStoreSnapshotIn,
  SharedStoreSnapshotOut,
} from '@sophie/shared';
