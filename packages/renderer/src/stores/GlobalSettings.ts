/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { defineGlobalSettingsModel, ThemeSource } from '@sophie/shared';
import { Instance } from 'mobx-state-tree';

import { getEnv } from './RendererEnv.js';
import Service from './Service.js';

const GlobalSettings = defineGlobalSettingsModel(Service).actions((self) => ({
  setSelectedServiceId(serviceId: string): void {
    getEnv(self).dispatchMainAction({
      action: 'set-selected-service-id',
      serviceId,
    });
  },
  setThemeSource(themeSource: ThemeSource): void {
    getEnv(self).dispatchMainAction({
      action: 'set-theme-source',
      themeSource,
    });
  },
  setShowLocationBar(showLocationBar: boolean): void {
    getEnv(self).dispatchMainAction({
      action: 'set-show-location-bar',
      showLocationBar,
    });
  },
  toggleLocationBar(): void {
    this.setShowLocationBar(!self.showLocationBar);
  },
}));

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
interface GlobalSettings extends Instance<typeof GlobalSettings> {}

export default GlobalSettings;

export type {
  GlobalSettingsSnapshotIn,
  GlobalSettingsSnapshotOut,
} from '@sophie/shared';
