/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import type { SophieRenderer } from '@sophie/shared';
import { applySnapshot, applyPatch, Instance, types } from 'mobx-state-tree';

import { getLogger } from '../utils/log.js';

import type GlobalSettings from './GlobalSettings.js';
import type RendererEnv from './RendererEnv.js';
import type Service from './Service.js';
import SharedStore from './SharedStore.js';

const log = getLogger('RendererStore');

const RendererStore = types
  .model('RendererStore', {
    shared: types.optional(SharedStore, {}),
  })
  .views((self) => ({
    get settings(): GlobalSettings {
      return self.shared.settings;
    },
    get services(): Service[] {
      return self.shared.services;
    },
  }));

/*
  eslint-disable-next-line @typescript-eslint/no-redeclare --
  Intentionally naming the type the same as the store definition.
*/
interface RendererStore extends Instance<typeof RendererStore> {}

export default RendererStore;

/**
 * Creates a new `RootStore` with a new environment and connects it to `ipc`.
 *
 * Changes to the `shared` store in the main process will be propagated to
 * the newly created store via `ipc`.
 *
 * @param ipc The `sophieRenderer` context bridge.
 * @returns A promise that resolves to the store once it was initialized.
 */
export async function createAndConnectRendererStore(
  ipc: SophieRenderer,
): Promise<RendererStore> {
  const env: RendererEnv = {
    dispatchMainAction: ipc.dispatchAction,
  };
  const store = RendererStore.create({}, env);

  try {
    await ipc.onSharedStoreChange({
      onSnapshot(snapshot) {
        applySnapshot(store.shared, snapshot);
      },
      onPatch(patch) {
        applyPatch(store.shared, patch);
      },
    });
  } catch (error) {
    log.error('Failed to connect to shared store', error);
  }

  return store;
}
