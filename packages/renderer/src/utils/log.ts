/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import loglevel, { Logger } from 'loglevel';
import prefix from 'loglevel-plugin-prefix';

if (import.meta.env?.DEV) {
  loglevel.setLevel('debug');
} else {
  // No devtools in production, so there's not point to log anything.
  loglevel.disableAll();
}

prefix.reg(loglevel);
prefix.apply(loglevel, {
  format(level, name, timestamp) {
    const timeStr = timestamp.toString();
    const nameStr = name === undefined ? '' : ` ${name}`;
    return `[${timeStr}] ${level}${nameStr}:`;
  },
});

export function getLogger(loggerName: string): Logger {
  return loglevel.getLogger(loggerName);
}

export function silenceLogger(): void {
  loglevel.disableAll();
  const loggers = loglevel.getLoggers();
  Object.values(loggers).forEach((logger) => {
    logger.disableAll();
  });
}
