/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import DoNotDisturbOnOutlinedIcon from '@mui/icons-material/DoNotDisturbOnOutlined';
import OpenInBrowserIcon from '@mui/icons-material/OpenInBrowser';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import Button from '@mui/material/Button';
import { useTheme } from '@mui/material/styles';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { Trans, useTranslation } from 'react-i18next';

import type Service from '../../stores/Service.js';

import NotificationBanner from './NotificationBanner.js';

function NewWindowBanner({
  service,
}: {
  service: Service;
}): JSX.Element | null {
  const { t } = useTranslation(undefined, {
    keyPrefix: 'banner.newWindow',
  });
  const { direction } = useTheme();

  const {
    popups,
    settings: { name },
  } = service;
  const { length: count } = popups;

  if (count === 0) {
    // eslint-disable-next-line unicorn/no-null -- React requires `null` to skip rendering.
    return null;
  }

  const url = popups[count - 1];

  return (
    <NotificationBanner
      severity="warning"
      icon={<OpenInNewIcon fontSize="inherit" />}
      onClose={() => service.dismissAllPopups()}
      buttons={
        <>
          <Button
            onClick={() => service.followPopup(url)}
            color="inherit"
            size="small"
            startIcon={
              direction === 'ltr' ? (
                <ArrowForwardIcon fontSize="inherit" />
              ) : (
                <ArrowBackIcon fontSize="inherit" />
              )
            }
          >
            {t('followLink')}
          </Button>
          <Button
            onClick={() => service.openPopupInExternalBrowser(url)}
            color="inherit"
            size="small"
            startIcon={<OpenInBrowserIcon />}
          >
            {t('openInExternalBrowser')}
          </Button>
          {count > 1 && (
            <>
              <Button
                onClick={() => service.openAllPopupsInExternalBrowser()}
                color="inherit"
                size="small"
                startIcon={<OpenInBrowserIcon />}
              >
                {t('openAllInExternalBrowser')}
              </Button>
              <Button
                onClick={() => service.dismissPopup(url)}
                color="inherit"
                size="small"
                startIcon={<DoNotDisturbOnOutlinedIcon fontSize="inherit" />}
              >
                {t('dismiss')}
              </Button>
            </>
          )}
        </>
      }
    >
      {count === 1 ? (
        <Trans
          i18nKey="messageSingleLink"
          t={t}
          components={[<strong>url</strong>]}
          values={{ name, url }}
        />
      ) : (
        <Trans
          i18nKey="messageMultipleLinks"
          count={count - 1}
          t={t}
          components={[<strong>url</strong>, <strong>count</strong>]}
          values={{ name, url, count: count - 1 }}
        />
      )}
    </NotificationBanner>
  );
}

export default observer(NewWindowBanner);
