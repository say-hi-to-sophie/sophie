/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import CableIcon from '@mui/icons-material/Cable';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import HomeIcon from '@mui/icons-material/HomeOutlined';
import Button from '@mui/material/Button';
import { SecurityLabelKind } from '@sophie/shared';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { useTranslation } from 'react-i18next';

import type Service from '../../stores/Service.js';

import NotificationBanner from './NotificationBanner.js';

function InsecureConnectionBanner({
  service,
}: {
  service: Service;
}): JSX.Element | null {
  const { t } = useTranslation(undefined, {
    keyPrefix: 'banner.insecureConnection',
  });

  const {
    canReconnectSecurely,
    hasError,
    securityLabel,
    settings: { name: serviceName },
  } = service;

  if (securityLabel !== SecurityLabelKind.NotSecureConnection || hasError) {
    // eslint-disable-next-line unicorn/no-null -- React requires `null` to skip rendering.
    return null;
  }

  return (
    <NotificationBanner
      severity="error"
      icon={<ErrorOutlineIcon fontSize="inherit" />}
      buttons={
        <>
          <Button
            onClick={() => service.goHome()}
            color="inherit"
            size="small"
            startIcon={<HomeIcon />}
          >
            {t('home', { serviceName })}
          </Button>
          {canReconnectSecurely && (
            <Button
              onClick={() => service.reconnectSecurely()}
              color="inherit"
              size="small"
              startIcon={<CableIcon />}
            >
              {t('reconnectSecurely')}
            </Button>
          )}
        </>
      }
    >
      {t('message')}
    </NotificationBanner>
  );
}

export default observer(InsecureConnectionBanner);
