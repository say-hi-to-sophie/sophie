/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import React from 'react';

import RendererStore from '../../stores/RendererStore.js';

import ServiceSwitcher from './ServiceSwitcher.js';
import ToggleDarkModeButton from './ToggleDarkModeButton.js';
import ToggleLocationBarButton from './ToggleLocationBarButton.js';

const SidebarRoot = styled(Box)(({ theme }) => ({
  flex: 0,
  display: 'flex',
  position: 'relative',
  overflow: 'hidden',
  flexDirection: 'column',
  alignItems: 'center',
  padding: `${theme.spacing(1)} 0`,
  gap: theme.spacing(1),
  backgroundColor:
    theme.palette.mode === 'dark'
      ? 'rgba(255, 255, 255, 0.09)'
      : 'rgba(0, 0, 0, 0.06)',
  minWidth: `calc(${theme.spacing(4)} + 36px)`,
  '::after': {
    content: '" "',
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    width: 0,
    zIndex: 100,
    borderLeft: `1px solid ${theme.palette.divider}`,
  },
}));

const SidebarFill = styled(Box)({
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'flex-start',
});

export default function Sidebar({
  store,
}: {
  store: RendererStore;
}): JSX.Element {
  return (
    <SidebarRoot component="aside">
      <ToggleLocationBarButton store={store} />
      <SidebarFill>
        <ServiceSwitcher store={store} />
      </SidebarFill>
      <ToggleDarkModeButton store={store} />
    </SidebarRoot>
  );
}
