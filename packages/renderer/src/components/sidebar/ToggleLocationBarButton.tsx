/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import IconChevronLeft from '@mui/icons-material/KeyboardDoubleArrowLeft';
import IconChevronRight from '@mui/icons-material/KeyboardDoubleArrowRight';
import CircularProgress from '@mui/material/CircularProgress';
import IconButton from '@mui/material/IconButton';
import { useTheme } from '@mui/material/styles';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { useTranslation } from 'react-i18next';

import type RendererStore from '../../stores/RendererStore.js';
import { getLocaltionBarID } from '../locationBar/LocationBar.js';

function ToggleLocationBarIcon({
  loading,
  show,
}: {
  loading: boolean;
  show: boolean;
}): JSX.Element {
  const { direction } = useTheme();
  if (loading) {
    return <CircularProgress color="inherit" size="1.5rem" />;
  }
  const left = direction === 'ltr' ? show : !show;
  return left ? <IconChevronLeft /> : <IconChevronRight />;
}

function ToggleLocationBarButton({
  store: {
    shared: { locationBarVisible, canToggleLocationBar },
    settings,
  },
}: {
  store: RendererStore;
}): JSX.Element {
  const { t } = useTranslation();
  const { selectedService } = settings;

  return (
    /* eslint-disable react/jsx-props-no-spreading -- Conditionally set the aria-controls prop. */
    <IconButton
      disabled={!canToggleLocationBar}
      aria-pressed={locationBarVisible}
      {...(selectedService === undefined
        ? {}
        : { 'aria-controls': getLocaltionBarID(selectedService) })}
      aria-label={t('toolbar.toggleLocationBar')}
      onClick={() => settings.toggleLocationBar()}
    >
      {/* eslint-enable react/jsx-props-no-spreading */}
      <ToggleLocationBarIcon
        loading={selectedService?.loading ?? false}
        show={locationBarVisible}
      />
    </IconButton>
  );
}

export default observer(ToggleLocationBarButton);
