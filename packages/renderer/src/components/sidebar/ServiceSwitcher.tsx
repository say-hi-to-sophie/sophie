/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import { alpha, styled } from '@mui/material/styles';
import type { TFunction } from 'i18next';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { useTranslation } from 'react-i18next';

import type RendererStore from '../../stores/RendererStore.js';
import type Service from '../../stores/Service.js';
import { getServicePanelID } from '../ServicePanel.js';

import ServiceIcon from './ServiceIcon.js';

const ServiceSwitcherRoot = styled(Tabs, {
  name: 'ServiceSwitcher',
  slot: 'Root',
})({
  // Move the indicator to the outer side of the window.
  '.MuiTabs-indicator': {
    left: 0,
    right: 'auto',
    width: 4,
  },
});

const ServiceSwitcherTab = styled(Tab, {
  name: 'ServiceSwitcher',
  slot: 'Tab',
})(({ theme }) => ({
  minWidth: 0,
  paddingInline: theme.spacing(2),
  transition: theme.transitions.create('background-color', {
    duration: theme.transitions.duration.shortest,
  }),
  '&.Mui-selected': {
    backgroundColor:
      theme.palette.mode === 'dark'
        ? alpha(theme.palette.text.primary, 0.1)
        : alpha(theme.palette.primary.light, 0.24),
  },
}));

function getServiceTitle(service: Service, t: TFunction) {
  const {
    settings: { name },
    directMessageCount,
    indirectMessageCount,
  } = service;
  let messagesText: string | undefined;
  if (indirectMessageCount > 0) {
    messagesText =
      directMessageCount > 0
        ? t('service.title.directAndIndirectMessageCount', {
            directMessageCount,
            indirectMessageCount,
          })
        : t('service.title.indirectMessageCount', {
            count: indirectMessageCount,
          });
  } else if (directMessageCount > 0) {
    messagesText = t('service.title.directMessageCount', {
      count: directMessageCount,
    });
  }
  if (messagesText === undefined) {
    return t('service.title.nameWithNoMessages', { name });
  }
  return t('service.title.nameWithMessages', { name, messages: messagesText });
}

function ServiceSwitcher({
  store: { settings, services },
}: {
  store: RendererStore;
}): JSX.Element {
  const { t } = useTranslation();

  const { selectedService } = settings;

  return (
    <ServiceSwitcherRoot
      variant="scrollable"
      orientation="vertical"
      value={selectedService === undefined ? false : selectedService.id}
      onChange={(_event, newValue: string) =>
        settings.setSelectedServiceId(newValue)
      }
    >
      {services.map((service) => (
        <ServiceSwitcherTab
          key={service.id}
          value={service.id}
          icon={<ServiceIcon service={service} />}
          aria-label={getServiceTitle(service, t)}
          aria-controls={getServicePanelID(service)}
        />
      ))}
    </ServiceSwitcherRoot>
  );
}

export default observer(ServiceSwitcher);
