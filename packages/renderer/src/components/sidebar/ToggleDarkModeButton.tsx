/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import DarkModeIcon from '@mui/icons-material/DarkMode';
import LightModeIcon from '@mui/icons-material/LightMode';
import IconButton from '@mui/material/IconButton';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { useTranslation } from 'react-i18next';

import type RendererStore from '../../stores/RendererStore.js';

function ToggleDarkModeButton({
  store: { shared },
}: {
  store: RendererStore;
}): JSX.Element {
  const { t } = useTranslation();

  return (
    <IconButton
      aria-label={t('toolbar.toggleDarkMode')}
      onClick={() => shared.toggleDarkMode()}
    >
      {shared.shouldUseDarkColors ? <LightModeIcon /> : <DarkModeIcon />}
    </IconButton>
  );
}

export default observer(ToggleDarkModeButton);
