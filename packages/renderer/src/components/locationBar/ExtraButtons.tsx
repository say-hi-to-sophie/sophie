/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import IconOpenInBrowser from '@mui/icons-material/OpenInBrowser';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { useTranslation } from 'react-i18next';

import type Service from '../../stores/Service.js';

function ExtraButtons({ service }: { service: Service }): JSX.Element {
  const { t } = useTranslation(undefined, {
    keyPrefix: 'toolbar',
  });

  return (
    <Box display="flex">
      <IconButton
        aria-label={t('openInBrowser')}
        disabled={service.currentUrl === undefined}
        onClick={() => service.openCurrentURLInExternalBrowser()}
      >
        <IconOpenInBrowser />
      </IconButton>
    </Box>
  );
}

export default observer(ExtraButtons);
