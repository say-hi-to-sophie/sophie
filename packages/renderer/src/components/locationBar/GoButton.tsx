/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import IconGo from '@mui/icons-material/Send';
import IconButton from '@mui/material/IconButton';
import React, { MouseEventHandler } from 'react';

import LocationInputAdornment from './LocationInputAdornment.js';

export default function GoButton({
  onClick,
  position,
}: {
  onClick: MouseEventHandler<HTMLButtonElement>;
  position: 'start' | 'end';
}): JSX.Element {
  return (
    <LocationInputAdornment position={position}>
      <IconButton
        aria-label="Go"
        color="inherit"
        onClick={onClick}
        sx={{
          color: 'text.primary',
          minWidth: '32px',
          height: '32px',
          paddingX: '6px',
          borderRadius: '16px',
        }}
      >
        <IconGo fontSize="small" />
      </IconButton>
    </LocationInputAdornment>
  );
}
