/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { styled } from '@mui/material/styles';
import { observer } from 'mobx-react-lite';
import React from 'react';

import type RendererStore from '../../stores/RendererStore.js';
import type Service from '../../stores/Service.js';

import ExtraButtons from './ExtraButtons.js';
import LocationTextField from './LocationTextField.js';
import NavigationButtons from './NavigationButtons.js';

export function getLocaltionBarID(service: Service): string {
  return `Sophie-${service.id}-LocationBar`;
}

const LocationBarRoot = styled('header', {
  name: 'LocationBar',
  slot: 'Root',
})(({ theme, hidden }) => ({
  display: hidden ? 'none' : 'flex',
  flexDirection: 'row',
  padding: theme.spacing(1),
  gap: theme.spacing(1),
  borderBottom: `1px solid ${theme.palette.divider}`,
}));

function LocationBar({
  store: {
    settings: { showLocationBar },
  },
  service,
}: {
  store: RendererStore;
  service: Service;
}): JSX.Element {
  const { alwaysShowLocationBar } = service;
  const locationBarVisible = showLocationBar || alwaysShowLocationBar;

  return (
    <LocationBarRoot
      id={getLocaltionBarID(service)}
      hidden={!locationBarVisible}
    >
      <NavigationButtons service={service} />
      <LocationTextField service={service} />
      <ExtraButtons service={service} />
    </LocationBarRoot>
  );
}

export default observer(LocationBar);
