/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { styled } from '@mui/material/styles';
import React from 'react';

import getAlertColor from './getAlertColor.js';

export type SplitResult =
  | {
      type: 'hostOnly';
      prefix: string;
      host: string;
      suffix: string;
    }
  | {
      type: 'wholeString';
      value: string;
    }
  | {
      type: 'empty';
    };

function splitUrl(urlString: string | undefined): SplitResult {
  if (urlString === undefined || urlString === '') {
    return { type: 'empty' };
  }
  let url: URL;
  try {
    url = new URL(urlString);
  } catch {
    return { type: 'wholeString', value: urlString };
  }
  const { protocol, host, username, password, pathname, search, hash } = url;
  if (host !== '') {
    return {
      type: 'hostOnly',
      prefix: `${protocol}//${
        username === ''
          ? ''
          : `${username}${password === '' ? '' : `:${password}`}@`
      }`,
      host,
      suffix: `${pathname}${search}${hash}`,
    };
  }
  return { type: 'wholeString', value: urlString };
}

const PrimaryFragment = styled('span', {
  name: 'LocationOverlayInput',
  slot: 'PrimaryFragment',
  shouldForwardProp: (prop) => prop !== 'alert',
})<{ alert: boolean }>(({ theme, alert }) => ({
  color: getAlertColor(theme, alert),
}));

const SecondaryFragment = styled('span', {
  name: 'LocationOverlayInput',
  slot: 'SecondaryFragment',
})(({ theme }) => ({
  color: theme.palette.text.secondary,
}));

export default function UrlOverlay({
  url,
  alert,
}: {
  url: string | undefined;
  alert: boolean;
}): JSX.Element {
  const splitResult = splitUrl(url);
  const { type } = splitResult;
  switch (type) {
    case 'hostOnly': {
      const { prefix, host, suffix } = splitResult;
      return (
        <>
          <SecondaryFragment>{prefix}</SecondaryFragment>
          <PrimaryFragment alert={alert}>{host}</PrimaryFragment>
          <SecondaryFragment>{suffix}</SecondaryFragment>
        </>
      );
    }
    case 'wholeString': {
      const { value } = splitResult;
      return <PrimaryFragment alert={alert}>{value}</PrimaryFragment>;
    }
    case 'empty':
      return <PrimaryFragment alert={alert} />;
    default:
      /* eslint-disable-next-line @typescript-eslint/restrict-template-expressions --
        Error handling for impossible case.
      */
      throw new Error(`Unexpected SplitResult: ${type}`);
  }
}
