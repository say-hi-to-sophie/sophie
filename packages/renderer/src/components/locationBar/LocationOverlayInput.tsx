/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { styled } from '@mui/material/styles';
import React, { forwardRef, ForwardedRef } from 'react';

const inputClassName = 'LocationOverlayInput-Input';

const overlayClassName = 'LocationOverlayInput-Overlay';

const clipClassName = 'LocationOverlayInput-Clip';

const LocationOverlayInputRoot = styled('div', {
  name: 'LocationOverlayInput',
  slot: 'Root',
  shouldForwardProp: (prop) => prop !== 'overlayVisible',
})<{ overlayVisible: boolean }>(({ theme, overlayVisible }) => {
  const itemStyle = {
    padding: '6px 0 7px 0',
    // Set text alignment explicitly so it can be flipped by `stylis-plugin-rtl`.
    textAlign: 'left',
  };
  return {
    display: 'flex',
    position: 'relative',
    flex: 1,
    [`.${inputClassName}`]: {
      ...itemStyle,
      color: overlayVisible ? 'transparent' : 'inherit',
    },
    [`.${overlayClassName}`]: {
      ...itemStyle,
      display: 'flex',
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      // Text rendering with selected transparent text works better on the bottom in light mode.
      zIndex: theme.palette.mode === 'dark' ? 999 : -999,
      pointerEvents: 'none',
      width: 'auto',
    },
    [`.${clipClassName}`]: {
      flex: 1,
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'clip',
    },
  };
});

export interface LocationOverlayInputProps
  extends React.HTMLProps<HTMLInputElement> {
  className?: string | undefined;

  overlayVisible?: boolean;

  overlay?: JSX.Element | undefined;
}

const LocationOverlayInput = forwardRef(
  (
    { overlayVisible, overlay, className, ...props }: LocationOverlayInputProps,
    ref: ForwardedRef<HTMLInputElement>,
  ) => (
    <LocationOverlayInputRoot overlayVisible={overlayVisible ?? false}>
      {/* eslint-disable react/jsx-props-no-spreading --
        Deliberately passing props through to the actual input element.
      */}
      <input
        ref={ref}
        className={`${className ?? ''} ${inputClassName}`}
        dir="ltr"
        {...props}
      />
      {/* eslint-enable react/jsx-props-no-spreading */}
      {overlayVisible && (
        <div aria-hidden className={`${className ?? ''} ${overlayClassName}`}>
          <div className={clipClassName} dir="ltr">
            {overlay}
          </div>
        </div>
      )}
    </LocationOverlayInputRoot>
  ),
);

LocationOverlayInput.displayName = 'LocationOverlayInput';

LocationOverlayInput.defaultProps = {
  className: undefined,
  overlayVisible: false,
  overlay: undefined,
};

export default LocationOverlayInput;
