/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import HttpsOutliedIcon from '@mui/icons-material/HttpsOutlined';
import NoEncrpytionIcon from '@mui/icons-material/NoEncryption';
import PublicIcon from '@mui/icons-material/Public';
import WarningIcon from '@mui/icons-material/Warning';
import { styled } from '@mui/material/styles';
import { SecurityLabelKind } from '@sophie/shared';
import React from 'react';
import { useTranslation } from 'react-i18next';

import LocationInputAdornment from './LocationInputAdornment.js';
import getAlertColor from './getAlertColor.js';

const SecurityLabelRoot = styled(LocationInputAdornment, {
  name: 'SecurityLabel',
  slot: 'Root',
  shouldForwardProp: (prop) => prop !== 'alert',
})<{ alert: boolean }>(({ theme, alert }) => ({
  padding: 6,
  color: getAlertColor(theme, alert),
  // Clicking on the security label should focus the text field instead.
  pointerEvents: 'none',
}));

const SecurityLabelText = styled('span', {
  name: 'SecurityLabel',
  slot: 'Text',
})(({ theme }) => ({
  marginLeft: theme.spacing(1),
  // Keep the same baseline as the input box text.
  paddingBottom: 1,
  fontWeight: theme.typography.fontWeightMedium,
  userSelect: 'none',
}));

export default function SecurityLabel({
  kind,
  changed,
  position,
}: {
  kind: SecurityLabelKind;
  changed: boolean;
  position: 'start' | 'end';
}): JSX.Element {
  const { t } = useTranslation();

  if (changed || kind === SecurityLabelKind.Empty) {
    return (
      <SecurityLabelRoot alert={false} position={position} aria-hidden>
        <PublicIcon fontSize="small" />
      </SecurityLabelRoot>
    );
  }
  if (kind === SecurityLabelKind.SecureConnection) {
    return (
      <SecurityLabelRoot
        alert={false}
        position={position}
        aria-label={t('securityLabel.secureConnection')}
      >
        <HttpsOutliedIcon fontSize="small" />
      </SecurityLabelRoot>
    );
  }
  const tslError =
    kind === SecurityLabelKind.NotSecureConnection ||
    kind === SecurityLabelKind.CertificateError;
  const icon = tslError ? (
    <NoEncrpytionIcon fontSize="small" />
  ) : (
    <WarningIcon fontSize="small" />
  );
  return (
    <SecurityLabelRoot alert position={position}>
      {icon}
      <SecurityLabelText>
        {t([`securityLabel.${kind}`, 'securityLabel.unspecific'])}
      </SecurityLabelText>
    </SecurityLabelRoot>
  );
}
