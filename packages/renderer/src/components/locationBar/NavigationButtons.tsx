/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import IconArrowBack from '@mui/icons-material/ArrowBack';
import IconArrowForward from '@mui/icons-material/ArrowForward';
import IconStop from '@mui/icons-material/Close';
import IconHome from '@mui/icons-material/HomeOutlined';
import IconRefresh from '@mui/icons-material/Refresh';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import { useTheme } from '@mui/material/styles';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { useTranslation } from 'react-i18next';

import type Service from '../../stores/Service.js';

function NavigationButtons({ service }: { service: Service }): JSX.Element {
  const { t } = useTranslation(undefined, {
    keyPrefix: 'toolbar',
  });
  const { direction } = useTheme();

  return (
    <Box display="flex">
      <IconButton
        aria-label={t('back')}
        disabled={!service.canGoBack}
        onClick={() => service.goBack()}
      >
        {direction === 'ltr' ? <IconArrowBack /> : <IconArrowForward />}
      </IconButton>
      <IconButton
        aria-label={t('forward')}
        disabled={!service.canGoForward}
        onClick={() => service.goForward()}
      >
        {direction === 'ltr' ? <IconArrowForward /> : <IconArrowBack />}
      </IconButton>
      {service.loading ? (
        <IconButton aria-label={t('stop')} onClick={() => service.stop()}>
          <IconStop />
        </IconButton>
      ) : (
        <IconButton
          aria-label={t('reload')}
          onClick={(event) => service.reload(event.shiftKey)}
        >
          <IconRefresh />
        </IconButton>
      )}
      <IconButton aria-label={t('home')} onClick={() => service.goHome()}>
        <IconHome />
      </IconButton>
    </Box>
  );
}

export default observer(NavigationButtons);
