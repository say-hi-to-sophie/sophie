/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import { observer } from 'mobx-react-lite';
import React from 'react';

import type RendererStore from '../stores/RendererStore.js';
import Service from '../stores/Service.js';

import BrowserViewPlaceholder from './BrowserViewPlaceholder.js';
import InsecureConnectionBanner from './banner/InsecureConnectionBanner.js';
import NewWindowBanner from './banner/NewWindowBanner.js';
import ErrorPage from './errorPage/ErrorPage.js';
import LocationBar from './locationBar/LocationBar.js';

const ServicePanelRoot = styled(Box, {
  shouldForwardProp: (prop) => prop !== 'hidden',
})(({ hidden }) => ({
  position: 'absolute',
  top: 0,
  left: 0,
  bottom: 0,
  right: 0,
  display: 'flex',
  overflow: 'hidden',
  visibility: hidden ? 'hidden' : 'visible',
  flexDirection: 'column',
  alignItems: 'stretch',
}));

export function getServicePanelID(service: Service): string {
  return `Sophie-${service.id}-ServicePanel`;
}

function ServicePanel({
  store,
  service,
}: {
  store: RendererStore;
  service: Service;
}): JSX.Element {
  const {
    settings: { selectedService },
  } = store;
  const {
    settings: { name },
  } = service;

  return (
    <ServicePanelRoot
      id={getServicePanelID(service)}
      role="tabpanel"
      aria-label={name}
      hidden={service !== selectedService}
    >
      <LocationBar store={store} service={service} />
      <InsecureConnectionBanner service={service} />
      <NewWindowBanner service={service} />
      <BrowserViewPlaceholder service={service}>
        <ErrorPage service={service} />
      </BrowserViewPlaceholder>
    </ServicePanelRoot>
  );
}

export default observer(ServicePanel);
