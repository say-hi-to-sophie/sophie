/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import {
  unstable_createMuiStrictModeTheme as createTheme,
  ThemeProvider as MuiThemeProvider,
} from '@mui/material/styles';
import { observer } from 'mobx-react-lite';
import React, { type ReactNode } from 'react';

import type RendererStore from '../stores/RendererStore.js';

function ThemeProvider({
  children,
  store: {
    shared: { shouldUseDarkColors, writingDirection },
  },
}: {
  children?: ReactNode;
  store: RendererStore;
}) {
  const theme = createTheme({
    direction: writingDirection,
    palette: shouldUseDarkColors
      ? {
          mode: 'dark',
          divider: 'rgba(255, 255, 255, 0.22)',
        }
      : {
          mode: 'light',
          divider: 'rgba(0, 0, 0, 0.24)',
        },
    typography: {
      fontFamily: 'system-ui',
      body1: {
        letterSpacing: 0,
      },
      body2: {
        letterSpacing: 0,
      },
      subtitle1: {
        letterSpacing: 0,
      },
      subtitle2: {
        letterSpacing: 0,
      },
      button: {
        fontSize: '1rem',
        textTransform: 'none',
        letterSpacing: 0,
        lineHeight: 1.5,
        fontWeight: 700,
      },
    },
    components: {
      MuiBadge: {
        styleOverrides: {
          standard: {
            // Reduce badge with to make the unread message badge with "99+" unread messages
            // fit within the sidebar. Applied for all badges for consistency.
            paddingLeft: 4,
            paddingRight: 4,
          },
        },
      },
      MuiIconButton: {
        styleOverrides: {
          sizeMedium: {
            padding: 6,
          },
        },
      },
      MuiButton: {
        styleOverrides: {
          sizeSmall: {
            fontSize: '0.875rem',
          },
          sizeLarge: {
            fontSize: '1rem',
          },
        },
      },
      MuiTab: {
        styleOverrides: {
          root: {
            fontWeight: 500,
          },
        },
      },
    },
  });

  return <MuiThemeProvider theme={theme}>{children}</MuiThemeProvider>;
}

ThemeProvider.defaultProps = {
  children: undefined,
};

export default observer(ThemeProvider);
