/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import { observer } from 'mobx-react-lite';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import type Service from '../stores/Service.js';

function WindowTitle({
  selectedService,
  devMode,
}: {
  selectedService: Service | undefined;
  devMode: boolean;
}): null {
  const { ready, t } = useTranslation(undefined, {
    useSuspense: false,
  });

  const {
    settings: { name: serviceName },
    title: serviceTitle,
  } = selectedService ?? { settings: { name: undefined }, title: undefined };

  useEffect(() => {
    if (!ready) {
      // Only set title once the translations have been loaded.
      return;
    }
    let title: string;
    if (serviceName === undefined) {
      title = t('title.noServiceName');
    } else if (serviceTitle === undefined) {
      title = t('title.withServiceName', { serviceName });
    } else {
      title = t('title.withServiceNameAndTitle', { serviceName, serviceTitle });
    }
    document.title = devMode ? t('title.devMode', { title }) : title;
  }, [devMode, ready, serviceName, serviceTitle, t]);

  // eslint-disable-next-line unicorn/no-null -- React requires `null` to skip rendering.
  return null;
}

export default observer(WindowTitle);
