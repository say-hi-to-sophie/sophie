/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import { observer } from 'mobx-react-lite';
import React, { useCallback } from 'react';

import type RendererStore from '../stores/RendererStore.js';

import ServicePanel from './ServicePanel.js';
import WindowTitle from './WindowTitle.js';
import Sidebar from './sidebar/Sidebar.js';

const AppRoot = styled(Box)({
  display: 'flex',
  overflow: 'hidden',
  flexDirection: 'row',
  alignItems: 'stretch',
  height: '100vh',
  width: '100vw',
});

const AppServiceArea = styled(Box)({
  flex: 1,
  height: '100%',
  position: 'relative',
});

function App({
  store,
  devMode,
}: {
  store: RendererStore;
  devMode: boolean;
}): JSX.Element {
  const {
    settings: { selectedService },
    shared: { services },
  } = store;

  const handleBackForwardMouseButtons = useCallback(
    (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      switch (event.button) {
        case 3:
          selectedService?.goBack();
          break;
        case 4:
          selectedService?.goForward();
          break;
        default:
          // Allow the event to propagate.
          return;
      }
      event.preventDefault();
      event.stopPropagation();
    },
    [selectedService],
  );

  return (
    <AppRoot
      onClick={handleBackForwardMouseButtons}
      onAuxClick={handleBackForwardMouseButtons}
    >
      <WindowTitle selectedService={selectedService} devMode={devMode} />
      <Sidebar store={store} />
      <AppServiceArea>
        {services.map((service) => (
          <ServicePanel key={service.id} store={store} service={service} />
        ))}
      </AppServiceArea>
    </AppRoot>
  );
}

export default observer(App);
