/*
 * Copyright (C)  2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import WarningAmberIcon from '@mui/icons-material/WarningAmber';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import React from 'react';
import { useTranslation } from 'react-i18next';

export default function TrustCertificateDialog({
  trust,
  onClick,
}: {
  trust: 'pending' | 'rejected' | 'accepted';
  onClick: () => void;
}): JSX.Element {
  const { t } = useTranslation(undefined, {
    keyPrefix: 'error.certificateError.details',
  });

  return (
    <>
      <Typography mt={2}>
        <WarningAmberIcon fontSize="inherit" /> {t('trustWarning')}
      </Typography>
      <Box mt={1}>
        <Button
          disabled={trust !== 'pending'}
          onClick={onClick}
          color="error"
          variant="outlined"
        >
          {t(`trustButton.${trust}`)}
        </Button>
      </Box>
    </>
  );
}
