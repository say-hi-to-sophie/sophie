/*
 * Copyright (C)  2021-2022 Kristóf Marussy <kristof@marussy.com>
 *
 * This file is part of Sophie.
 *
 * Sophie is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import Box from '@mui/material/Box';
import throttle from 'lodash-es/throttle';
import React, { ReactNode, useCallback, useRef } from 'react';

import Service from '../stores/Service.js';

function BrowserViewPlaceholder({
  service,
  children,
}: {
  service: Service;
  children?: ReactNode;
}): JSX.Element {
  // eslint-disable-next-line react-hooks/exhaustive-deps -- react-hooks doesn't support `throttle`.
  const onResize = useCallback(
    throttle(([entry]: ResizeObserverEntry[]) => {
      if (entry) {
        const { x, y, width, height } = entry.target.getBoundingClientRect();
        service.setBrowserViewBounds({
          x: Math.round(x),
          y: Math.round(y),
          width: Math.round(width),
          height: Math.round(height),
        });
      }
    }, 100),
    [service],
  );

  const resizeObserverRef = useRef<ResizeObserver | undefined>();

  const ref = useCallback(
    (element: HTMLElement | null) => {
      if (resizeObserverRef.current !== undefined) {
        resizeObserverRef.current.disconnect();
      }
      if (element === null) {
        resizeObserverRef.current = undefined;
        return;
      }
      resizeObserverRef.current = new ResizeObserver(onResize);
      resizeObserverRef.current.observe(element);
    },
    [onResize],
  );

  return (
    <Box ref={ref} flex={1} overflow="auto">
      {children}
    </Box>
  );
}

BrowserViewPlaceholder.defaultProps = {
  children: undefined,
};

export default BrowserViewPlaceholder;
