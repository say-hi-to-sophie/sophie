/* eslint-disable no-process-env */
/* eslint-env node */

import { builtinModules } from 'node:module';
import path from 'node:path';

import react from '@vitejs/plugin-react';

import { banner, chrome } from '../../config/buildConstants.js';
import fileUrlToDirname from '../../config/fileUrlToDirname.js';

const thisDir = fileUrlToDirname(import.meta.url);

const mode = process.env.MODE || 'development';

const isDevelopment = mode === 'development';

/**
 * @type {import('vite').UserConfig}
 * @see https://vitejs.dev/config/
 */
export default {
  /** @type {import('vite').LogLevel} */
  logLevel: 'info',
  mode,
  root: thisDir,
  cacheDir: path.join(thisDir, '../../.vite'),
  plugins: [
    react({
      babel: {
        // Gets rid of deoptimization warnings for large chunks.
        // We don't need to minify here, because the output of babel
        // will get passed to esbuild anyways.
        compact: false,
        minified: false,
      },
    }),
  ],
  base: '',
  optimizeDeps: {
    exclude: ['@sophie/shared'],
  },
  define: {
    __DEV__: JSON.stringify(isDevelopment), // For mobx
  },
  resolve: {
    alias: {
      '@mui/base': '@mui/base/modern',
      '@mui/lab': '@mui/lab/modern',
      '@mui/material': '@mui/material/modern',
      '@mui/system': '@mui/system/modern',
    },
  },
  build: {
    target: chrome,
    assetsDir: '.',
    outDir: 'dist',
    emptyOutDir: true,
    sourcemap: isDevelopment,
    minify: !isDevelopment,
    brotliSize: false,
    rollupOptions: {
      external: ['mst-middlewares', 'remotedev', ...builtinModules],
      output: {
        banner,
      },
    },
  },
};
