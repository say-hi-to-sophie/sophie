import baseConfig from '../../config/jest.config.base.js';

/** @type {import('@jest/types').Config.InitialOptions} */
export default {
  ...baseConfig,
  rootDir: 'src',
  testEnvironment: 'jsdom',
};
