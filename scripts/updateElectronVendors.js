import { execSync } from 'node:child_process';
import { writeFile } from 'node:fs/promises';
import path from 'node:path';

import electronPath from 'electron';

import fileUrlToDirname from '../config/fileUrlToDirname.js';

const thisDir = fileUrlToDirname(import.meta.url);

/**
 * Returns versions of electron vendors
 * The performance of this feature is very poor and can be improved
 * @see https://github.com/electron/electron/issues/28006
 *
 * @returns {NodeJS.ProcessVersions}
 */
function getVendors() {
  const output = execSync(
    `${electronPath.toString()} -p "JSON.stringify(process.versions)"`,
    {
      env: { ELECTRON_RUN_AS_NODE: '1' },
      encoding: 'utf8',
    },
  );

  // eslint-disable-next-line @typescript-eslint/no-unsafe-return -- Read untyped output.
  return JSON.parse(output);
}

/**
 * Generates the `.browserlistrc` and `.electron-vendors.cache.json` files.
 *
 * @returns Promise<void>
 */
function updateVendors() {
  const electronRelease = getVendors();

  const nodeMajorVersion = electronRelease.node.split('.')[0];
  const chromeMajorVersion =
    electronRelease.v8.split('.')[0] + electronRelease.v8.split('.')[1];

  const browserslistrcPath = path.join(thisDir, '../.browserslistrc');

  return Promise.all([
    writeFile(
      path.join(thisDir, '../.electron-vendors.cache.json'),
      `${JSON.stringify(
        {
          chrome: chromeMajorVersion,
          node: nodeMajorVersion,
        },
        undefined,
        2,
      )}\n`,
    ),

    writeFile(browserslistrcPath, `Chrome ${chromeMajorVersion}\n`, 'utf8'),
  ]);
}

updateVendors().catch((error) => {
  console.error(error);
  process.exit(1);
});
