import path from 'node:path';

import { build as esbuildBuild } from 'esbuild';
import { build as viteBuild } from 'vite';

import fileUrlToDirname from '../config/fileUrlToDirname.js';

const thisDir = fileUrlToDirname(import.meta.url);

/**
 * @param {string} packageName
 * @returns {Promise<import('esbuild').BuildResult>}
 */
async function buildPackageEsbuild(packageName) {
  /** @type {{ default: import('esbuild').BuildOptions }} */
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment -- Read untyped config file.
  const { default: config } = await import(
    `../packages/${packageName}/esbuild.config.js`
  );
  return esbuildBuild(config);
}

/**
 * @param {string} packageName
 * @returns {Promise<unknown>}
 */
function buildPackageVite(packageName) {
  return viteBuild({
    configFile: path.join(thisDir, `../packages/${packageName}/vite.config.js`),
  });
}

function buildAll() {
  const buildServiceShared = buildPackageEsbuild('service-shared');
  const buildShared = buildPackageEsbuild('shared');
  return Promise.all([
    Promise.all([buildServiceShared, buildShared]).then(() =>
      buildPackageEsbuild('main'),
    ),
    buildServiceShared
      .then(() => buildPackageEsbuild('service-inject'))
      .then(() => buildPackageEsbuild('service-preload')),
    buildShared.then(() =>
      Promise.all([
        buildPackageEsbuild('preload'),
        buildPackageVite('renderer'),
      ]),
    ),
  ]);
}

buildAll().catch((error) => {
  console.error(error);
  process.exit(1);
});
