const { app } = require('electron');

if (process.env.DISABLE_GPU === 'true') {
  app.disableHardwareAcceleration();
}

app.on('window-all-closed', () => {
  // Handle event to avoid exiting the application in watch mode.
});

// @ts-expect-error -- `jest-cli` has no typescript declaration.
require('jest-cli/bin/jest');
