
const { Arch } = require('electron-builder');
const { FuseV1Options, FuseVersion } = require('@electron/fuses');

const burnFuses = require('./config/burnFuses.cjs');
const enableWaylandAutoDetection = require('./config/enableWaylandAutoDetection.cjs');

/**
 * @type {import('electron-builder').Configuration}
 * @see https://www.electron.build/configuration/configuration
 */
const config = {
  directories: {
    output: 'dist',
    buildResources: 'buildResources',
  },
  files: [
    'packages/main/dist/**',
    'packages/preload/dist/**',
    'packages/renderer/dist/**',
    'packages/service-preload/dist/**',
    'locales/**',
    // Do not ship with source maps.
    '!**/*.map',
  ],
  afterPack(context) {
    /*
    * Enables chromium cookie encryption and disables options that could be
    * used to execute arbitrary code in the main process to circumvent cookie encryption:
    */
    return burnFuses(context, {
      version: FuseVersion.V1,
      resetAdHocDarwinSignature:
        context.electronPlatformName === 'darwin' && context.arch === Arch.arm64,
      [FuseV1Options.RunAsNode]: false,
      [FuseV1Options.EnableCookieEncryption]: true,
      [FuseV1Options.EnableNodeOptionsEnvironmentVariable]: false,
      [FuseV1Options.EnableNodeCliInspectArguments]: false,
      // TODO: Revisit this: IF set to `true` the packaged app doesn't start up on macos (x86)
      [FuseV1Options.EnableEmbeddedAsarIntegrityValidation]: false,
      [FuseV1Options.OnlyLoadAppFromAsar]: true,
    });
  },
  async afterSign(context) {
    if (context.electronPlatformName === 'linux') {
      await enableWaylandAutoDetection(context);
    }
  }
};

module.exports = config;
