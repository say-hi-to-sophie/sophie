---
title: Architecture
---

# Purpose

Sophie is designed to

- display _services_, i.e., web pages, most of which are messaging web applications,
- allow the user to quickly switch between services and save resources by displaying multiple services in the same desktop applications,
- provide integrations for services, e.g., to manage unread message counts and notification in a single place.

Integrations for services are provided by pluggable _recipes_ to allow adding support for new services easily.
A recipe may come directly from Sophie, a third party (under AGPLv3 licensing), or be created by the user themselves.

# Electron

Sophie is built on the [Electron](https://www.electronjs.org/) application framework, which in turn uses the [Chromium](https://www.chromium.org/) browser to execute javascript applications.

## Displaying foreign web pages

Electron makes two facilities readily available to display foreign web pages:

- The easier solution that mimics the HTML `<iframe>` tag is [`<webview>`](https://www.electronjs.org/docs/latest/api/webview-tag/).
  However, it is deprecated and has some [critical bugs](https://github.com/electron/electron/issues/25469) when loading content with modern web security headers.
- The new solution is the [`BrowserView`](https://www.electronjs.org/docs/latest/api/browser-view/), which is the approach taken by Sophie.
  The `BrowserView` is an overlay over the main browser window, so _Sophie will not be able render UI elements over foreign web pages_.
  While this is somewhat limiting, it allows us to take advantage of modern Electron APIs and coding practices.

## Processes and isolation

An Electron application is split into a _main process_ and a number of _renderer processes_:

- The main process has access to NodeJS APIs for direct file system access.
  It is also the only process that can manipulate browser windows and `BrowserView` instances.
  Therefore, most of Sophie's logic must be implemented in the main process.
- The renderer processes are sandboxed processes responsible for rendering and executing web pages, both local and foreign.
  In turn, they are split into an _isolated world_ and a _main world_.
  The isolated world (world id 999) can use [IPC](https://www.electronjs.org/docs/latest/api/ipc-renderer/) to communicate with the main process.
  The only way to expose APIs to the main world is through the [`contextBridge`](https://www.electronjs.org/docs/latest/api/context-bridge).
  The main world (world id 0) then can execute untrusted code (e.g., foreign web pages) and still stay sandboxed.
  In Sophie, both the main window user interface (a local web page) and services are loaded in isolated renderer processes.

While not strictly necessary for local-only content, we use content isolation also for Sophie's UI to provide defense in depth against injection attacks.

**Heads up:**
The terminology here is a bit confusing.
The _main process_ refers to the most privileged process of an Electron application.
The _main world_ of a renderer process contains the code ran with the least privileges within an Electron application.

**TODO:**
We should figure out how recipes should fit into this model, and how much do we trust recipes.
Since recipes interact with the corresponding service, a compromised service can always compromise the service's data.
However, we should strive to minimize the compromise of other services' data by a compromised recipe.
This suggest running inside a renderer process only, maybe even within an isolated context.

# Data flow

Sophie is a reactive application with unidirectional data flow for easy debugging.

## Stores

_Stores_ defined and maintained by [mobx-state-tree](https://github.com/mobxjs/mobx-state-tree) act as a single source of truth.
IPC messages that invoke store actions are validated by [https://github.com/colinhacks/zod] at context boundaries (both in the isolated world and in the main process).

In the main process, the [`MainStore`](https://gitlab.com/say-hi-to-sophie/sophie/-/blob/main/packages/main/src/stores/MainStore.ts) holds application state.

Within the `MainStore`, changes to the [`SharedStore`](https://gitlab.com/say-hi-to-sophie/sophie/-/blob/main/packages/shared/src/stores/SharedStore.ts) are pushed to the UI process.
Thus it can hold application state relevant to displaying the UI.

The [`Config`](https://gitlab.com/say-hi-to-sophie/sophie/-/blob/main/packages/shared/src/stores/Config.ts) in the `SharedStore` holds the application configuration, including the list of services to be displayed.
It is synchronized with the `settings.json` file in user data directory, which should be human-readable and -editable to facilitate debugging and other advanced use cases.

In the UI renderer process, the [`RendererStore`](https://gitlab.com/say-hi-to-sophie/sophie/-/blob/main/packages/renderer/src/stores/RendererStore.ts) hold the UI sate.
It contains a read-only copy of the `SharedStore`.
Any actions of the `RendererStore` that should affect the shared state have to go through the IP API exposed from the isolated world of the UI renderer process.

To reduce the amount of code injected into service frames, service renderer processes contain no stores.
Instead, they purely rely on IPC messages to invoke actions in the main process (but they are not notified of the result).

## Reactions

In the main process, _reactions_ react to `MainStore` changes by invoking Electron APIs and subscribe to Electron events in order to invoke `MainStore` actions.

For better testability, reactions may rely on _infrastructure services_ (wrappers) abstracting away the underlying Electron APIs.
Each infrastructure of the service has to come with a TypeScript interface and at least one implementation.
In the tests, the default implementations of the interfaces are replaced by mocks.

The infrastructure services and reactions are instantiated and connected to the `MainStore` in the [composition root](https://gitlab.com/say-hi-to-sophie/sophie/-/blob/main/packages/main/src/init.ts).

## React

In the UI renderer process, the UI is reactively derived from the `RendererStore` via [mobx-react-lite](https://github.com/mobxjs/mobx) and [React](https://reactjs.org/).
We use the [MUI](https://mui.com/) UI library for UI components.

In particular, the [`BrowserViewPlaceholder`](https://gitlab.com/say-hi-to-sophie/sophie/-/blob/main/packages/renderer/src/components/BrowserViewPlaceholder.tsx) synchronizes the location where the service `BrowserView` should be renderer to the main process.
We must take care not to render anything in this area, since it will be entirely covered by the service.

# Packages

The code of Sophie is distributed between different Node packages according to how they are loaded into the application.

All packages except the renderer package are tree-shaken and bundled with [esbuild](https://esbuild.github.io/) for quicker loading.
The web application in the renderer packages is tree-shaken and bundled with [vite](https://vitejs.dev/).

<dl>
  <dt><a href="https://gitlab.com/say-hi-to-sophie/sophie/-/tree/main/packages/main">main</a></dt>
  <dd> Contains the code running in the main process. Bundled into a CommonJS package with external dependencies on Electron APIs.</dd>
  <dt><a href="https://gitlab.com/say-hi-to-sophie/sophie/-/tree/main/packages/preload">preload</a></dt>
  <dd>Exposes the IPC APIs for the renderer package from the isolated world of the UI renderer process. Bundled into a CommonJS package with external dependencies on the Electron IPC API.</dd>
  <dt><a href="https://gitlab.com/say-hi-to-sophie/sophie/-/tree/main/packages/renderer">renderer</a></dt>
  <dd> Contains the user interface of Sophie running in the main world of the UI renderer process.</dd>
  <dt><a href="https://gitlab.com/say-hi-to-sophie/sophie/-/tree/main/packages/service-inject">service-inject</a></dt>
  <dd>Injected into the main world of the service renderer process to shim browser APIs, such as screen sharing (overwriting properties of the <code>window</code> object requires running in the main world). Bundled into an IIFE (immediately invoked function expression) to avoid clashing with the service's code.</dd>
  <dt><a href="https://gitlab.com/say-hi-to-sophie/sophie/-/tree/main/packages/service-preload">service-preload</a></dt>
  <dd>Exposes the IPC APIs for recipes and the service-inject package from the isolated world of the service renderer process. Bundled into a CommonJS package with external dependencies on the Electron IPC API.</dd>
  <dt><a href="https://gitlab.com/say-hi-to-sophie/sophie/-/tree/main/packages/service-shared">service-shared</a></dt>
  <dd>IPC message definitions shared between the main and service-preload packages. During build, it gets bundled into an ESM package with all dependencies external, and is used within the build of dependent packages.</dd>
  <dt><a href="https://gitlab.com/say-hi-to-sophie/sophie/-/tree/main/packages/shared">shared</a></dt>
  <dd>Store and IPC message definitions shared between the main, preload, and renderer packages. During build, it gets bundled into an ESM package with all dependencies external, and is used within the build of dependent packages.</dd>
</dl>
